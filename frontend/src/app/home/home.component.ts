import { Component, OnInit, HostBinding } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass'],
})
export class HomeComponent implements OnInit {
  constructor(private router: Router) { }
  ngOnInit() {}
  @HostBinding('class.home-container') true;
  FindRoad(event){
    this.router.navigate(['/road-list']);
  }
  FindHotel(event){
    this.router.navigate(['/hotel-list']);
  }
  PlanTrip(event){
    this.router.navigate(['/plan-trip']);
  }
}
