import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthorizationService} from '../apiconect/authorization.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ApiService} from '../apiconect/api.service';

export interface Rate {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.sass']
})
export class CommentsListComponent implements OnInit {
  public isEmpty = true;
  public isErrorRate = false;
  public isErrorText = false;

  private content = '';
  public errorMessageRate = 'Ocena wymagana';
  public errorMessageText = 'Treść komentarza  nie może byc pusta';
  @Input() commentList;
  @Input() hotelId;
  @Input() roadId;
  selectedValue = '';
  rates: Rate[] = [
    {value: '1', viewValue: '1'},
    {value: '2', viewValue: '2'},
    {value: '3', viewValue: '3'},
    {value: '4', viewValue: '4'},
    {value: '5', viewValue: '5'},
  ];

  constructor(private authService: AuthorizationService,
              private apiservice: ApiService,
              private http: HttpClient) {
  }

  ngOnInit() {
    // if (this.commentList.length !== 0) {
    //   console.log(this.commentList.length);
    //   this.isEmpty = false;
    // }
  }

  typed(event, number) {
    if (number === 2) {
      console.log(event);
      this.content = event;
    }

  }

  addComment() {
    console.log('tu jestem');
    if (!this.authService.getIsAuthorize()) {
      alert('Zaloguj sie żeby zapisać hotel');
      return;
    } else {
      if (this.content === '') {
        console.log('tu jestem');

        this.isErrorText = true;
        return;
      }
      if (this.selectedValue === '') {
        console.log('tu jestem');

        this.isErrorRate = true;
        return;
      }
      if (this.hotelId !== '0') {
        console.log('tu jestem');

        const comment = {
          title: '',
          content: this.content,
          rate: this.selectedValue,
          user_login: localStorage.getItem('user_login'),
          hotel_id: this.hotelId,
        };
        this.saveComentHToBase(comment);
        // this.commentList
        this.commentList.push(comment);
      }
      if (this.roadId !== '0') {
        console.log('tu jestem');
        const comment = {
          title: '',
          content: this.content,
          rate: this.selectedValue,
          user_login: localStorage.getItem('user_login'),
          road_id: this.roadId,
        };
        this.saveComentRToBase(comment);
        // this.commentList
        this.commentList.push(comment);
        this.content = '';
        this.selectedValue = '';
      }
    }
  }

  saveComentHToBase(comment) {

    const headers = new HttpHeaders()
      .set('Authorization', 'Token ' + this.authService.getToken());


    this.http
      .post(this.apiservice.commentHotelCreate, comment, {headers: headers})
      .subscribe(
        res => {

          alert('Pomyślnie dodano komentarz ');
        },
        error => {
          console.log(error);
        }
      );
  }

  saveComentRToBase(comment) {

    const headers = new HttpHeaders()
      .set('Authorization', 'Token ' + this.authService.getToken());


    this.http
      .post(this.apiservice.commentRoadCreate, comment, {headers: headers})
      .subscribe(
        res => {

          alert('Pomyślnie dodano komentarz ');
        },
        error => {
          console.log(error);
        }
      );
  }
}
