import {Component, OnInit} from '@angular/core';
import {ApiService} from '../apiconect/api.service';
import {HttpClient} from '@angular/common/http';
import {FormControl} from '@angular/forms';
import {debounceTime, switchMap} from 'rxjs/operators';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-newtripform',
  templateUrl: './new-trip-form.component.html',
  styleUrls: ['./new-trip-form.component.sass']
})
export class NewTripFormComponent implements OnInit {
  private sPoint: Point;
  public form = new TripForm(null, null);
  private query: string;
  private ePoint: Point;
  public hotelCityList: Sugestion [] = null;
  public errorMessage = 'Nie dodano miasta, spróbuj ponownie';
  public isError = false;
  public isErrorSec = false;
  startForm: FormControl;
  endForm: FormControl;
  sugestion: Sugestion;

  cities: Sugestion [] = [
    ['Brak sugestii']
  ];
  citiesSec: Sugestion [] = [
    ['Brak sugestii']
  ];
  public nextForm = false;
  private disableAddCity = false;


  constructor(private apiservice: ApiService,
              private http: HttpClient,
              private dialogRef: MatDialogRef<NewTripFormComponent>
  ) {

  }

  initStartForm() {
    this.startForm = new FormControl();
    this.startForm
      .valueChanges
      .pipe(
        switchMap(value => this.cities)
      );
  }

  ngOnInit() {
    this.initStartForm();
    this.endForm = new FormControl();
    this.endForm
      .valueChanges
      .pipe(
        debounceTime(200),
        switchMap(value => this.citiesSec)
      );
  }

  submit() {
    this.isError = false;
    this.isErrorSec = false;
    if (this.form.start === null) {
      this.isError = true;
    }
    if (this.form.end === null) {
      this.isErrorSec = true;
    }
    if (this.form.start !== null && this.form.end !== null) {
      this.cities = null;
      this.initStartForm();
      this.nextForm = true;
    }
  }

  selectSuggest(item, number) {
    this.isError = false;
    this.isErrorSec = false;
    if (item === undefined || item === null) {
      if (number === 0) {
        this.isError = true;

      } else {
        this.isErrorSec = true;

      }

    } else {
      if (number === 0) {
        this.sPoint = new Point(item['1'], item['2'], item['0']);
        this.form.start = this.sPoint;
        if (this.endForm.disabled) {
          this.ePoint = new Point(item['1'], item['2'], item['0']);
          this.form.end = this.ePoint;

        }
        this.form.start = item['0'];

      } else {
        this.ePoint = new Point(item['1'], item['2'], item['0']);
        this.form.end = this.ePoint;
        this.form.end = item['0'];
      }
    }

  }

  typed(event, number) {

    this.query = event + '&format=json&addressdetails=1';

    this.http.get(this.apiservice.API_NOMINATIM + this.query).subscribe(
      res => {
        this.cities = [];
        this.citiesSec = [];
        for (let i = 0; i < 10; i++) {
          if (number === 0) {
            try {
              this.cities.push(
                [res[i].display_name,
                  res[i].lon,
                  res[i].lat,
                  res[i].address.city,
                  res[i].address.country_code,
                  res[i].address.country]);
            } catch (e) {
            }
          } else {
            try {
              this.citiesSec.push(
                [res[i].display_name,
                  res[i].lon,
                  res[i].lat,
                  res[i].address.city,
                  res[i].address.country_code,
                  res[i].address.country]);
            } catch (e) {
            }
          }


        }
      },
      error => {
      }
    );

  }

  setStartAsEnd() {
    this.ePoint =
      new Point(this.sPoint.lon, this.sPoint.lat, this.sPoint.name);
    this.form.end = this.sPoint;
    this.endForm.disable();
  }

  setEndNull() {

    this.ePoint = null;
    this.form.end = null;
    this.endForm.enable();
  }

  setItem(item) {

    this.isError = false;
    this.disableAddCity = false;
    if (item === undefined || item === null) {
      this.isError = true;
      this.disableAddCity = true;


    } else {
      this.sugestion = item;
      this.disableAddCity = false;

    }
  }


  addCity() {
    if (this.disableAddCity === true || this.sugestion === null || this.sugestion === undefined) {
      this.isError = true;
      return 0;
    }
    this.isError = false;
    if (this.hotelCityList === null) {
      this.hotelCityList = [];
    }
    this.hotelCityList.push(this.sugestion);
    this.sugestion = null;
    this.cities = null;
    this.initStartForm();
  }

  createTrip() {
    const tripData = {
      startPoint: this.sPoint,
      endPoint: this.ePoint,
      cityTab: this.hotelCityList
    };
    console.log(tripData);
    this.dialogRef.close(tripData);

  }

  closeDialog() {
    this.dialogRef.close(null);

  }
}


class Point {
  constructor(
    public lon: number,
    public lat: number,
    public name: string,
  ) {
  }
}

class TripForm {
  constructor(
    public start: Point,
    public end: Point,
  ) {
  }
}

class Sugestion {
  constructor(
    text: string,
    lon: number,
    lat: number,
    city: string,
    country_code: string,
    country: string
  ) {

  }
}
