import {Component, OnInit} from '@angular/core';
import {MatDialogRef, NativeDateAdapter} from '@angular/material';
import {FormControl} from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import * as _moment from 'moment';
// @ts-ignore
import {default as _rollupMoment} from 'moment';

const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MM YYYY',
  },
};

@Component({
  selector: 'app-namedialog',
  templateUrl: './namedialog.component.html',
  styleUrls: ['./namedialog.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})


export class NamedialogComponent implements OnInit {
  saveNameForm: FormControl;
  public errorMessage = 'Pole jest nazwa jest puste. Podaj nazwę!';
  public isError = false;
  private typedName: string;
  public isDateError = false;
  public date: any;
  private dateToReturn: string;

  constructor(private dialog: MatDialogRef<NamedialogComponent>) {
    this.typedName = null;
  }

  events: string[] = [];


  addEvent(type: string, event: any) {
    let monthStr;
    let dayStr;
    const month = parseInt(event.value['_i'].month, 0) + 1;
    if (month.toString().length === 1) {
      monthStr = '0' + month.toString();
    } else {
      monthStr = month.toString();
    }
    if (event.value['_i'].date.toString().length === 1) {
      dayStr = '0' + event.value['_i'].date.toString();
    } else {
      dayStr = event.value['_i'].date.toString();

    }

    this.dateToReturn = event.value['_i'].year + '-' + monthStr + '-' + dayStr;

  }

  ngOnInit() {
    this.date = new FormControl(moment());
    this.saveNameForm = new FormControl();
  }

  typed(data) {
    this.typedName = data;
  }

  returnName() {
    console.log(this.typedName);
    if (this.typedName === null || this.typedName === '' || this.typedName === undefined) {
      this.isError = true;
      return;
    }
    if (this.date === null || this.date === '' || this.date === undefined) {
      this.isDateError = true;
      return;
    }
    this.dialog.close({
      name: this.typedName,
      date: this.dateToReturn
    });
  }
}
