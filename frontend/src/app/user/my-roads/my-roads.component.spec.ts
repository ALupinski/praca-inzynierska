import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRoadsComponent } from './my-roads.component';

describe('MyRoadsComponent', () => {
  let component: MyRoadsComponent;
  let fixture: ComponentFixture<MyRoadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyRoadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyRoadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
