import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../apiconect/api.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MatDialog} from '@angular/material';
import {AuthorizationService} from '../../apiconect/authorization.service';
import * as geolib from 'geolib';
import {DatePipe} from '@angular/common';

declare var ol: any;

@Component({
  selector: 'app-my-tickets',
  templateUrl: './my-roads.component.html',
  styleUrls: ['./my-roads.component.sass']
})
export class MyRoadsComponent implements OnInit {
  public sPoint: Point;
  public ePoint: Point;
  map: any;
  public roadList = [];
  private isToken: boolean;
  private iconFeature: any;
  private routeLayer = new ol.layer.Vector();
  private markerSource = new ol.source.Vector();
  public loading = false;
  private markerStyle = new ol.style.Style({
    image: new ol.style.Circle({
      radius: 7,
      stroke: new ol.style.Stroke({
        color: '#333333'
      }),
      fill: new ol.style.Fill({
        color: '#333333' // attribute colour
      })
    }),
  });
  public showSteps = false;
  private actualStepNumber: number;
  private steps: any;
  private stepsArrayLength: number;
  actualStep: any;
  private actualGeometry: any;
  private actualDistance: number;
  private actualTime: number;
  private requestedRute: any;
  private showRouteResoult: boolean;
  public isEmpty = false;
  private actualRouteId: number;

  public showComments = false;
  public comments: any;
  public roadId: string;

  public oldObject = [];
  public activeObject = [];
  private roadListHelper: any;

  constructor(private apiservice: ApiService,
              private datePipe: DatePipe,
              private http: HttpClient,
              private authService: AuthorizationService) {
  }


  ngOnInit() {
    this.getUserRoads();
    if (this.roadList.length === 0) {
      this.isEmpty = true;
    }
    if (localStorage.getItem('token') !== null) {
      this.isToken = true;
    }
    this.initMap();
  }

  initMap() {
    this.map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        }),
        new ol.layer.Vector({
          source: this.markerSource,
          style: this.markerStyle,
        }),
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([20, 50]),
        zoom: 4.6,
        projection: 'EPSG:3857',
      }),
    });
    setTimeout(() => this.map.updateSize(), 100);
    this.initRouteLayer();
  }

  initRouteLayer() {
    this.routeLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: [new ol.Feature({
          geometry: new ol.geom.LineString([], 'XY'),
          name: 'Line'
        })]
      }),
      style: new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: '#333333',
          width: 4
        })
      })
    });
    this.routeLayer.setZIndex(5);
    this.map.addLayer(this.routeLayer);
  }

  nextStep() {
    if (this.actualStepNumber !== this.stepsArrayLength) {
      this.actualStepNumber++;
      this.actualStep = this.steps[this.actualStepNumber];
      this.actualStep.instruction = this.returnInstruction(this.actualStep.type, this.actualStep.name, this.actualStep.distance);
      this.setPositionMapPoint();
    }
  }

  previousStep() {
    if (this.actualStepNumber !== 0) {
      this.actualStepNumber--;
      this.actualStep = this.steps[this.actualStepNumber];
      this.actualStep.instruction = this.returnInstruction(this.actualStep.type, this.actualStep.name, this.actualStep.distance);
      this.setPositionMapPoint();
    }
  }

  setPositionMapPoint() {
    const num = this.actualStep['way_points'][0];
    this.setCenter(this.actualGeometry[num][0], this.actualGeometry[num][1], 17);
  }

  addMapPoint(lon, lat, id) {
    this.iconFeature = new ol.Feature({
      geometry: new ol.geom.Point(ol.proj.transform([parseFloat(lon), parseFloat(lat)], 'EPSG:4326',
        'EPSG:3857')),
      name: 'Null Island',
      population: 4000,
      rainfall: 500,
      id: id
    });

    this.markerSource.addFeature(this.iconFeature);

  }

  returnInstruction(type, name, dist) {
    let instruction: string;
    switch (type) {
      case 0: {
        instruction = 'Za ' + dist + ' metrów skręć w lewo ';
        break;
      }
      case 1: {
        instruction = 'Za ' + dist + ' metrów skręć w prawo ';

        break;
      }
      case 2: {
        instruction = 'Za ' + dist + ' metrów skręć ostro w lewo ';
        break;
      }
      case 3: {
        instruction = 'Za ' + dist + ' metrów skręć ostro w prawo ';
        break;
      }
      case 4: {
        instruction = 'Za ' + dist + ' metrów skręć lekko w lewo ';
        break;
      }
      case 5: {
        instruction = 'Za ' + dist + ' metrów skręć lekko w prawo ';
        break;
      }
      case 6: {
        instruction = 'Jedź dalej prosto ';
        break;
      }
      case 7: {
        instruction = 'Na rondzie drugi zjazd ';
        break;
      }
      case 8: {
        instruction = 'Na rondzie trzeci zjazd ';
        break;
      }
      case 9: {
        instruction = 'Na rondzie czwarty zjazd ';
        break;
      }
      case 10: {
        instruction = 'Dotarłeś do celu ';
        break;
      }
      case 11: {
        instruction = 'Kieruj się na północ przez ' + dist + ' metrów';
        break;
      }
      case 12: {
        instruction = 'Trzymaj się lewej strony ';
        break;
      }
      case 13: {
        instruction = 'Trzymaj się prawej strony ';
        break;
      }
    }
    if (name !== '') {
      instruction = instruction + 'na ' + name;
    }
    return instruction;
  }

  setCenter(lon, lat, zoom) {
    const view = this.map.getView();
    view.setCenter(ol.proj.fromLonLat([parseFloat(String(lon)), parseFloat(String(lat))]));
    view.setZoom(zoom);
  }

  setActualData(json) {
    this.actualGeometry = json['0'].geometry;
    this.steps = json['0'].segments['0'].steps;
    this.stepsArrayLength = json['0'].segments['0'].steps.length;
    this.actualStepNumber = 0;
    this.actualStep = this.steps[this.actualStepNumber];
    this.actualStep.instruction = this.returnInstruction(this.actualStep.type, this.actualStep.name, this.actualStep.distance);
  }

  getCenterBetween() {
    let center: any;
    center = geolib.getCenter([
      {latitude: this.sPoint.lat, longitude: this.sPoint.lon},
      {latitude: this.ePoint.lat, longitude: this.ePoint.lon}
    ]);
    // {latitude: "53.295895", longitude: "19.859769"}
    this.setCenter(center.longitude, center.latitude, 6);
  }

  showRoute(road) {
    this.showComments = true;
    this.roadId = road.road_id;
    this.actualRouteId = road.road_id;
    this.clearLayer();
    this.loading = true;
    let profile = 'driving-car';
    this.sPoint = new Point(road.slon, road.slat, road.sPointName);
    this.ePoint = new Point(road.elon, road.elat, road.ePointName);
    this.addMapPoint(this.sPoint.lon, this.sPoint.lat, 0);
    this.addMapPoint(this.ePoint.lon, this.ePoint.lat, 0);
    this.getCenterBetween();
    const pref = this.returnRoadType(road.geometryName);
    if (pref === 'recommended') {
      profile = 'driving-hgv';
    }
    this.getComments(this.roadId);

    this.getWay(pref, profile);
  }

  getWay(pref, profile) {
    this.http.get(this.apiservice.getOpenRouteUrl(
      this.sPoint.lon,
      this.sPoint.lat,
      this.ePoint.lon,
      this.ePoint.lat,
      pref,
      profile))
      .debounceTime(400)
      .subscribe(
        res => {
          this.requestedRute = res['routes'];
          this.loading = false;
          this.setActualData(this.requestedRute);
          this.importRoute(this.actualGeometry);
          this.getTimeAndDistance();
          this.showRouteResoult = true;
          this.showSteps = true;
        },
        error => {
          this.responseErrorCatch(error);
        }
      );
  }

  clearLayer() {
    if (this.routeLayer.getSource() !== null) {
      const features = this.routeLayer.getSource().getFeatures();
      features.forEach((feature) => {
        this.routeLayer.getSource().removeFeature(feature);
      });
      this.markerSource.clear(true);
    }
  }

  getTimeAndDistance() {
    this.actualDistance = parseFloat(this.requestedRute['0'].summary['distance']) / 1000;
    this.actualTime = parseFloat(this.requestedRute['0'].summary['duration']) / 60;
    this.actualDistance = Math.round(this.actualDistance);
    this.actualTime = Math.round(this.actualTime);

  }

  responseErrorCatch(error) {
    let errorMessage: any;
    this.loading = false;
    switch (error['error']['error']['message']) {
      case 'Request parameters exceed the server configuration limits. ' +
      'By dynamic weighting, the approximated distance of a route segment must not be greater than 300000.0 meters.': {
        errorMessage = 'Wybrane punkty są za daleko od siebie';
        break;
      }
      default: {
        errorMessage = error['error']['error']['message'];
        break;
      }
    }
    alert(errorMessage);
  }

  importRoute(geometry) {
    const features = this.routeLayer.getSource().getFeatures();
    features.forEach((feature) => {
      this.routeLayer.getSource().removeFeature(feature);
    });
    const pointarray = [];
    for (let i = 0; i < geometry.length; i++) {
      pointarray.push([geometry[i][0], geometry[i][1]]);
    }
    const polyline = new ol.geom.LineString(pointarray);
    polyline.transform('EPSG:4326', 'EPSG:3857');

    this.routeLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: [new ol.Feature({
          geometry: polyline,
          name: 'Line'
        })]
      }),
      style: new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: '#333333',
          width: 3
        })
      })
    });
    this.routeLayer.setZIndex(5);
    this.map.addLayer(this.routeLayer);

  }

  returnRoadType(actualGeometryName) {
    let pref;
    switch (actualGeometryName) {
      case 'F': {
        pref = 'fastest';
        break;
      }
      case 'R': {
        pref = 'recommended';
        break;
      }
      case 'S': {
        pref = 'shortest';

        break;
      }
      default: {
        pref = 'recommended';

        break;
      }
    }
    return pref;
  }

  getUserRoads() {
    this.loading = true;

    const headers = new HttpHeaders()
      .set('Authorization', 'Token ' + this.authService.getToken());


    this.http
      .get(this.apiservice.roadUserListUrl, {headers: headers})
      .subscribe(
        res => {
          console.log(res);
          this.roadList = res['user_road'];
          this.roadListHelper = res['user_road'];
          this.isEmpty = false;
          const date = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
          for (const ob of this.roadList) {
            console.log(date);
            if (date >= ob.usage_date) {
              this.oldObject.push(ob);
            } else {
              this.activeObject.push(ob);
            }
          }
          this.loading = false;
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  deleteRoad(id) {
    this.loading = true;

    const headers = new HttpHeaders()
      .set('Authorization', 'Token ' + this.authService.getToken());

    const body = {
      road_id: id
    };

    this.http
      .post(this.apiservice.roadDeleteUrl, body, {headers: headers})
      .subscribe(
        res => {
          console.log(res);
          this.isEmpty = false;

          this.loading = false;
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  getComments(id) {
    this.loading = true;

    this.http
      .get(this.apiservice.commentRoadList)
      .subscribe(
        res => {
          // this.comments
          const com = res['roads_comments'];
          // if (this.comments.length === 0) {
          //   this.comments = [];
          // }
          this.comments = [];
          for (const comment of com) {
            console.log(id);
            console.log(comment.road_id);
            if (comment.road_id === id) {
              this.comments.push(comment);
            }
          }

          console.log(res);
          this.loading = false;
        },
        error => {
          this.comments = [];
          this.loading = false;
        }
      );
  }

  sortByNameAZ() {
    this.roadList.sort((a, b) => a.name.localeCompare(b.name));
    // this.sortTooltipMessage = 'Sortuj od Z do A.';
  }

  sortByNameZA() {
    this.roadList.sort((a, b) => a.name.localeCompare(b.name));
    // this.sortTooltipMessage = 'Sortuj od A do Z.';
    this.roadList.reverse();
  }

  sortByAddDateO() {
    this.roadList.sort(
      (a, b) => a.created_date.substr(0, 16).replace('T', ' ').localeCompare(b.created_date.substr(0, 16).replace('T', ' ')));

  }

  sortByAddDateY() {
    this.roadList.sort(
      (a, b) => a.created_date.substr(0, 16).replace('T', ' ').localeCompare(b.created_date.substr(0, 16).replace('T', ' ')));
    this.roadList.reverse();
  }
  sortByUsageDateO() {
    this.roadList.sort(
      (a, b) => a.usage_date.substr(0, 16).replace('T', ' ').localeCompare(b.usage_date.substr(0, 16).replace('T', ' ')));

  }

  sortByUsageDateN() {
    this.roadList.sort(
      (a, b) => a.usage_date.substr(0, 16).replace('T', ' ').localeCompare(b.usage_date.substr(0, 16).replace('T', ' ')));
    this.roadList.reverse();
  }
  tabChange(event) {
    console.log(event.tab.textLabel);
    switch (event.tab.textLabel) {
      case 'Wszystkie': {
        this.roadList = this.roadListHelper;
        break;
      }
      case 'Archiwalne': {
        this.roadList = this.oldObject;
        break;
      }
      case 'Aktywne': {
        this.roadList = this.activeObject;
        console.log(this.activeObject);
        break;
      }
      default: {
        this.roadList = this.roadListHelper;
        break;
      }
    }
  }
}

class Point {
  constructor(
    public lon: number,
    public lat: number,
    public name: string,
  ) {
  }
}
