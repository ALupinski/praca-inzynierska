import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthorizationService} from '../apiconect/authorization.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.sass']
})
export class UserComponent implements OnInit {
  NameEmpty: boolean;
  uname: string;
  surname: string;
  user_name: string;
  constructor(private router: Router, private auth_service: AuthorizationService) { }

  ngOnInit() {
    if (this.auth_service.getToken() === null) {
      this.router.navigate(['/home']);
      // toast
    }
  }
  AccountDetails(event) {
    this.router.navigate(['/user']);
  }
  Roads(event) {
    this.router.navigate(['/user/my-roads']);
  }
  Hotels(event) {
    this.router.navigate(['/user/my-hotels']);
  }
  MyTrips(event) {
    this.router.navigate(['/user/my-trips']);
  }
}
