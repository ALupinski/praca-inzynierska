import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyReservedHotelsComponent } from './my-reserved-hotels.component';

describe('MyReservedHotelsComponent', () => {
  let component: MyReservedHotelsComponent;
  let fixture: ComponentFixture<MyReservedHotelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyReservedHotelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyReservedHotelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
