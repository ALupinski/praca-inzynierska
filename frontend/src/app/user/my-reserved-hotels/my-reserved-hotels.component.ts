import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {ApiService} from '../../apiconect/api.service';
import {AuthorizationService} from '../../apiconect/authorization.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppComponent} from '../../app.component';
import {switchMap} from 'rxjs/operators';
import {DatePipe} from '@angular/common';

declare var ol: any;


@Component({
  selector: 'app-my-reserved-hotels',
  templateUrl: './my-reserved-hotels.component.html',
  styleUrls: ['./my-reserved-hotels.component.sass']
})
export class MyReservedHotelsComponent implements OnInit {

  public loading = false;
  public showHotelData = false;
  private iconFeature: any;
  private markerSource = new ol.source.Vector();
  private markerStyle = new ol.style.Style({
    image: new ol.style.Icon(({
      anchorXUnits: 'fraction',
      scale: 0.08,
      anchorYUnits: 'pixels',
      src: '../../assets/images/hotel.svg'
    }))
  });
  public hotelList = [];
  public hotelInfo: any;
  map: any;
  private Point: Point;

  public floors: any;
  public rooms: any;
  public qualitty: any;
  public address: any;
  public phone: any;
  public fax: any;
  public amenity: any;
  public description: any;
  public creditcard: any;
  public translationText: any;
  public translationTextDescr: string;
  public hotelName: any;
  public errorMessage: string;
  public resultIsEmpty = false;
  private translateCity: string;
  private actualHotel: any;
  private isToken: boolean;
  public isEmpty = false;
  public comments: any;
  public hotelId: any;
  private hotelListHelper: any;
  private oldObject = [];
  private activeObject = [];

  constructor(
    private datePipe: DatePipe,
    private apiservice: ApiService,
    private authServicee: AuthorizationService,
    private http: HttpClient, private navBar: AppComponent) {

  }

  ngOnInit() {
    this.getUserHotels();
    if (this.hotelList.length === 0) {
      this.isEmpty = true;
    }
    if (localStorage.getItem('token') !== null) {
      this.isToken = true;
    }
    this.initMap();
  }

  initMap() {
    this.map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        }),
        new ol.layer.Vector({
          source: this.markerSource,
          style: this.markerStyle,
        }),
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([20, 50]),
        zoom: 4.6,
        projection: 'EPSG:3857',
      }),
    });
    setTimeout(() => this.map.updateSize(), 100);
  }

  getHotelInfo(id) {
    this.loading = true;

    const headers = new HttpHeaders()
      .set('Authorization', 'Bearer ' + this.apiservice.getSabreToken())
      .set('Content-Type', 'application/json');

    const body = {
      GetHotelDescriptiveInfoRQ: {
        HotelRefs: {
          HotelRef: [{
            HotelCode: id
          }]
        },
        DescriptiveInfoRef: {
          PropertyInfo: true,
          LocationInfo: true,
          Amenities: true,
          Descriptions: {
            Description: [{
              Type: 'Dining'
            }]
          },
          Airports: true,
          AcceptedCreditCards: true
        }
      }
    };

    this.http
      .post(this.apiservice.getSabreDescriptionUrl(), body, {headers: headers})
      .subscribe(
        res => {
          this.hotelInfo = res['GetHotelDescriptiveInfoRS'].HotelDescriptiveInfos['HotelDescriptiveInfo'];

          this.prepareDataToDisplay();
          this.getComments(id);


        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  setCenter(lon, lat, zoom) {
    const view = this.map.getView();
    view.setCenter(ol.proj.fromLonLat([parseFloat(String(lon)), parseFloat(String(lat))]));
    view.setZoom(zoom);
  }


  addMapPoint(lon, lat, name, id) {
    this.iconFeature = new ol.Feature({
      geometry:
        new ol.geom.Point(
          ol.proj.transform([parseFloat(lon), parseFloat(lat)],
            'EPSG:4326',
            'EPSG:3857')
        ),
      name: name,
      id: id,
    });

    this.markerSource.addFeature(this.iconFeature);

  }

  showHotel(hotel) {
    this.clearLayer();
    console.log(hotel);
    this.setCenter(hotel['lon'], hotel['lat'], 17);
    this.loading = true;
    this.getHotelInfo(hotel['id']);
    this.hotelId = hotel['Id'];
    this.hotelName = hotel['Name'];
    this.actualHotel = hotel;
    this.addMapPoint(hotel['lon'], hotel['lat'], '', 0);

  }

  clearLayer() {
    this.markerSource.clear(true);

  }


  prepareDataToDisplay() {
    try {
      this.floors = this.hotelInfo['0'].PropertyInfo['Floors'];
    } catch (e) {
      this.floors = 'Brak informacji';
    }

    try {
      this.rooms = this.hotelInfo['0'].PropertyInfo['Rooms'];
    } catch (e) {
      this.rooms = 'Brak informacji';
    }
    try {
      this.qualitty = this.hotelInfo['0'].PropertyInfo['PropertyQualityInfo'].PropertyQuality['0'].content;
    } catch (e) {
      this.qualitty = 'Brak informacji';
      console.log(e);
    }
    try {
      this.address = this.hotelInfo['0'].LocationInfo['Address'].AddressLine1;
    } catch (e) {
      this.address = 'Brak informacji';
      console.log(e);
    }
    try {
      this.phone = this.hotelInfo['0'].LocationInfo['Contact'].Phone;
    } catch (e) {
      this.phone = 'Brak informacji';
      console.log(e);
    }
    try {
      this.fax = this.hotelInfo['0'].LocationInfo['Contact'].Fax;
    } catch (e) {
      this.fax = 'Brak informacji';
      console.log(e);
    }
    let string = '';

    try {
      this.amenity = this.hotelInfo['0'].Amenities['Amenity'];
      for (const am of this.amenity) {
        string = string + am.Description + ',';
      }
    } catch (e) {
      this.translationText = 'Brak informacji';
      console.log(e);
    }
    if (this.translationText === 'Brak informacji') {
      string = 'Brak informacji';
    }

    try {
      this.creditcard = this.hotelInfo['0'].AcceptedCreditCards['CreditCard'];
    } catch (e) {
      this.creditcard = 'Brak informacji';
      console.log(e);
    }

    try {
      this.description = this.hotelInfo['0'].Descriptions['Description'][0].Text['content'];

    } catch (e) {
      this.description = 'Brak informacji';
      console.log(e);
    }

    this.newTranslate(string, 0);
    this.newTranslate(this.qualitty, 1);

    this.newTranslate(this.description, 2);

  }

  creditCardImg(name) {
    let url = '';
    switch (name) {
      case 'AMERICAN EXPRESS': {
        url = '../../assets/images/americanexpress.svg';
        break;
      }
      case 'MASTERCARD': {
        url = '../../assets/images/mastercard.svg';
        break;
      }
      case 'DINERS CLUB CARD': {
        url = '../../assets/images/dinersclub.svg';
        break;
      }
      case 'JCB CREDIT CARD': {
        url = '../../assets/images/jcb.svg';
        break;
      }
      case 'VISA': {
        url = '../../assets/images/visa.svg';
        break;
      }
      default: {
        break;
      }
    }
    return url;
  }

  newTranslate(text, num) {
    const url = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');

    const key = 'trnsl.1.1.20190126T164009Z.975ec213deedc2d3.7b16f5fe6ca9a9521c03c0467d2c22681c9aaecd';

    const body = 'key=' + key + '&text=' + text + '&lang=pl';

    this.http
      .post(url, body, {headers: headers})
      .subscribe(
        res => {
          if (num === 2) {
            this.translationTextDescr = res['text'].toString().toLocaleLowerCase();
            // this.translationTextDescr = this.translationTextDescr.toLowerCase();
            this.loading = false;
            this.showHotelData = true;
          } else if (num === 1) {
            this.qualitty = res['text'];
          } else {
            this.translationText = res['text'];
          }
        },
        error => {
          console.log(error);
        }
      );

  }

  newTranslateCity(text) {
    const url = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');

    const key = 'trnsl.1.1.20190126T164009Z.975ec213deedc2d3.7b16f5fe6ca9a9521c03c0467d2c22681c9aaecd';

    const body = 'key=' + key + '&text=' + text + '&lang=en';

    this.http
      .post(url, body, {headers: headers})
      .subscribe(
        res => {
          this.translateCity = res['text'].toString();
          this.loading = false;
        },
        error => {
          console.log(error);
        }
      );

  }

  private getUserHotels() {
    this.loading = true;

    const headers = new HttpHeaders()
      .set('Authorization', 'Token ' + this.authServicee.getToken());


    this.http
      .get(this.apiservice.hotelUserListUrl, {headers: headers})
      .subscribe(
        res => {
          console.log(res);
          this.hotelList = res['user_hotels'];
          this.hotelListHelper = res['user_hotels'];
          this.isEmpty = false;
          const date = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
          for (const ob of this.hotelList) {
            console.log(date);
            if (date >= ob.usage_date) {
              this.oldObject.push(ob);
            } else {
              this.activeObject.push(ob);
            }
          }
          this.loading = false;
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  getComments(hotelId) {
    this.loading = true;

    this.http
      .get(this.apiservice.commentHotelList + hotelId)
      .subscribe(
        res => {
          this.comments = res['hotels_comments'];
          if (this.comments.length === 0) {
            this.comments = [];
          }
          console.log(this.comments);
          this.loading = false;
        },
        error => {
          this.comments = [];
          this.loading = false;
        }
      );
  }

  sortByNameAZ() {
    this.hotelList.sort((a, b) => a.name.localeCompare(b.name));
    // this.sortTooltipMessage = 'Sortuj od Z do A.';
  }

  sortByNameZA() {
    this.hotelList.sort((a, b) => a.name.localeCompare(b.name));
    // this.sortTooltipMessage = 'Sortuj od A do Z.';
    this.hotelList.reverse();
  }

  sortByAddDateO() {
    this.hotelList.sort(
      (a, b) => a.created_date.substr(0, 16).replace('T', ' ').localeCompare(b.created_date.substr(0, 16).replace('T', ' ')));

  }

  sortByAddDateY() {
    this.hotelList.sort(
      (a, b) => a.created_date.substr(0, 16).replace('T', ' ').localeCompare(b.created_date.substr(0, 16).replace('T', ' ')));
    this.hotelList.reverse();
  }
  sortByUsageDateO() {
    this.hotelList.sort(
      (a, b) => a.usage_date.substr(0, 16).replace('T', ' ').localeCompare(b.usage_date.substr(0, 16).replace('T', ' ')));

  }

  sortByUsageDateN() {
    this.hotelList.sort(
      (a, b) => a.usage_date.substr(0, 16).replace('T', ' ').localeCompare(b.usage_date.substr(0, 16).replace('T', ' ')));
    this.hotelList.reverse();
  }
  tabChange(event) {
    console.log(event.tab.textLabel);
    switch (event.tab.textLabel) {
      case 'Wszystkie': {
        this.hotelList = this.hotelListHelper;
        break;
      }
      case 'Archiwalne': {
        this.hotelList = this.oldObject;
        break;
      }
      case 'Aktywne': {
        this.hotelList = this.activeObject;
        console.log(this.activeObject);
        break;
      }
      default: {
        this.hotelList = this.hotelListHelper;
        break;
      }
    }
  }
}

class Point {
  constructor(
    public lon: number,
    public lat: number,
    public name: string,
  ) {
  }
}


