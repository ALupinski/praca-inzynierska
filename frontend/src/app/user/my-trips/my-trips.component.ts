import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ApiService} from '../../apiconect/api.service';
import {AuthorizationService} from '../../apiconect/authorization.service';
import {debounce} from 'rxjs/operators';
import * as geolib from 'geolib';
import {DatePipe} from '@angular/common';

declare var ol: any;

@Component({
  selector: 'app-my-trips',
  templateUrl: './my-trips.component.html',
  styleUrls: ['./my-trips.component.sass']
})
export class MyTripsComponent implements OnInit {
  public loading = false;
  public tripList = [];
  private isToken: boolean;

  public sPoint: Point;
  public ePoint: Point;
  map: any;
  public roadList = [];
  private iconFeature: any;
  private routeLayer = new ol.layer.Vector();
  private markerSource = new ol.source.Vector();
  private markerStyle = new ol.style.Style({
    image: new ol.style.Circle({
      radius: 7,
      stroke: new ol.style.Stroke({
        color: '#333333'
      }),
      fill: new ol.style.Fill({
        color: '#333333' // attribute colour
      })
    }),
  });
  public showSteps = false;
  private actualStepNumber: number;
  private steps: any;
  private stepsArrayLength: number;
  actualStep: any;
  private actualGeometry: any;
  private actualDistance: number;
  private actualTime: number;
  private requestedRute: any;
  private showRouteResoult: boolean;
  public isEmpty = false;
  private actualRouteId: number;
  public actualTrip: any;
  private actualHotelList: any;
  public actualRouteList: any;
  private HotelDataTab = [];
  public cityCordinates = [];
  public RouteListData = [];
  public showList = true;
  private actStartName: any;
  private actEndName: any;
  public isData = false;
  public hotelName: any;
  public actualHotel: any;
  public hotelInfo: any;
  public floors: any;
  public rooms: any;
  public qualitty: any;
  public address: any;
  public phone: any;
  public fax: any;
  public amenity: any;
  public translationText: string;
  public creditcard: any;
  public description: any;
  public translationTextDescr: string;
  public showHotelData: boolean;

  public oldObject = [];
  public activeObject = [];
  private tripListHelper: any;

  //roadsComment
  public showCommentsRoad = false;
  public showCommentsHotel = false;
  public comments: any;
  public roadId: string;
  public hotelId: string;

  constructor(private apiservice: ApiService,
              private http: HttpClient,
              private datePipe: DatePipe,
              private authService: AuthorizationService) {
  }

  ngOnInit() {
    this.getUserTrips();
    if (this.tripList.length === 0) {
      this.isEmpty = true;
    }
    if (localStorage.getItem('token') !== null) {
      this.isToken = true;
    }
    this.initMap();
  }

  initMap() {
    this.map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        }),
        new ol.layer.Vector({
          source: this.markerSource,
          style: this.markerStyle,
        }),
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([20, 50]),
        zoom: 4.6,
        projection: 'EPSG:3857',
      }),
    });
    setTimeout(() => this.map.updateSize(), 100);
    this.initRouteLayer();
  }

  initRouteLayer() {
    this.routeLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: [new ol.Feature({
          geometry: new ol.geom.LineString([], 'XY'),
          name: 'Line'
        })]
      }),
      style: new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: '#333333',
          width: 4
        })
      })
    });
    this.routeLayer.setZIndex(5);
    this.map.addLayer(this.routeLayer);
  }

  getUserTrips() {
    this.loading = true;

    const headers = new HttpHeaders()
      .set('Authorization', 'Token ' + this.authService.getToken());


    this.http
      .get(this.apiservice.tripUserListUrl, {headers: headers})
      .subscribe(
        res => {
          console.log(res);
          this.tripList = res['user_trips'];
          this.tripListHelper = res['user_trips'];
          const date = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
          for (const ob of this.tripList) {
            console.log(date);
            if (date >= ob.usage_date) {
              this.oldObject.push(ob);
            } else {
              this.activeObject.push(ob);
            }
          }
          this.isEmpty = false;
          this.loading = false;
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  deleteTrip() {

  }

  showTrip(trip) {
    this.actualTrip = trip;
    this.actualHotelList = trip['hotels'];
    this.actualRouteList = trip['roads'];
    this.actualHotelList.sort((a, b) => a.hotel_number_trip.localeCompare(b.hotel_number_trip));
    this.actualRouteList.sort((a, b) => a.road_number_in_trip.localeCompare(b.road_number_in_trip));
    this.preparePointsData();
    // get all steps
    this.showList = false;
  }

  showListAgain() {
    this.actualTrip = [];
    this.actualHotelList = [];
    this.actualRouteList = [];
    this.HotelDataTab = [];
    this.cityCordinates = [];
    this.RouteListData = [];
    this.showList = true;
  }

  getHotelInfo(id, isLast, numer) {
    this.loading = true;

    const headers = new HttpHeaders()
      .set('Authorization', 'Bearer ' + this.apiservice.getSabreToken())
      .set('Content-Type', 'application/json');

    const body = {
      GetHotelDescriptiveInfoRQ: {
        HotelRefs: {
          HotelRef: [{
            HotelCode: id
          }]
        },
        DescriptiveInfoRef: {
          PropertyInfo: true,
          LocationInfo: true,
          Amenities: true,
          Descriptions: {
            Description: [{
              Type: 'Dining'
            }]
          },
          Airports: true,
          AcceptedCreditCards: true
        }
      }
    };

    this.http
      .post(this.apiservice.getSabreDescriptionUrl(), body, {headers: headers})
      .subscribe(
        res => {
          const hotelInfo = res['GetHotelDescriptiveInfoRS'].HotelDescriptiveInfos['HotelDescriptiveInfo'];
          this.HotelDataTab.push(hotelInfo);
          const lonlat = {
            numerontrip: parseInt(numer, 10),
            name: hotelInfo['0'].LocationInfo.Address.CityName,
            lon: hotelInfo['0'].LocationInfo.Longitude,
            lat: hotelInfo['0'].LocationInfo.Latitude,
            hotelInfo: hotelInfo,
            hotelName: hotelInfo['0'].HotelInfo.HotelName,
            id:id
          };
          this.cityCordinates[numer] = lonlat;
          if (isLast) {
            // this.cityCordinates.sort((a, b) => a.numerontrip.toString().localeCompare(b.numerontrip.toString()));
            console.log(this.cityCordinates, '    tu ');
            this.getRoutesData();

          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  getFirstRoute() {
    let profile = 'driving-car';
    const road_type = this.returnRoadType(this.actualRouteList['0'].road_type);
    if (road_type === 'recommended') {
      profile = 'driving-hgv';
    }

    this.getWay(
      this.actualTrip.start_name,
      this.cityCordinates['0'].name,
      road_type,
      profile,
      parseFloat(String(this.actualTrip.start_lat)),
      parseFloat(String(this.actualTrip.start_lon)),
      this.cityCordinates['0'].lat,
      this.cityCordinates['0'].lon,
      -1,
      false
    );
  }

  getLastRoute() {
    const last = this.cityCordinates.length - 1;
    let profile = 'driving-car';
    const road_type = this.returnRoadType(this.actualRouteList[last].road_type);
    if (road_type === 'recommended') {
      profile = 'driving-hgv';
    }

    console.log('wywollll');
    this.getWay(
      this.actualTrip.end_name,
      this.cityCordinates[last].name,
      road_type,
      profile,
      this.cityCordinates[last].lat,
      this.cityCordinates[last].lon,
      parseFloat(String(this.actualTrip.end_lat)),
      parseFloat(String(this.actualTrip.end_lon)),
      last,
      2
    );
  }

  getRoutesData() {
    this.loading = true;
    this.getFirstRoute();
    for (let i = 0; i < this.cityCordinates.length - 1; i++) {
      let profile = 'driving-car';
      let isLast = 0;
      if (i === this.cityCordinates.length - 2) {
        isLast = 1;
      }
      const road_type = this.returnRoadType(this.actualRouteList[i].road_type);
      if (road_type === 'recommended') {
        profile = 'driving-hgv';
      }

      const road_number_in_trip = this.actualRouteList[i].road_number_in_trip;

      const sname = this.cityCordinates[i].name;
      const ename = this.cityCordinates[i + 1].name;
      this.getWay(
        sname,
        ename,
        road_type,
        profile,
        this.cityCordinates[i].lat,
        this.cityCordinates[i].lon,
        this.cityCordinates[i + 1].lat,
        this.cityCordinates[i + 1].lon,
        i,
        1,
      );

    }

  }

  getWay(sname, ename, pref, profile, slat, slon, elat, elon, rnumber, isLast) {
    this.http.get(this.apiservice.getOpenRouteUrl(
      slon,
      slat,
      elon,
      elat,
      pref,
      profile))
      .debounceTime(400)
      .subscribe(
        res => {
          this.requestedRute = res['routes'];
          const timedist = this.getTimeAndDistance(res['routes']);
          const routeData = {
            route_number: rnumber,
            res_routes: res['routes'],
            geometry: res['routes']['0'].geometry,
            steps: res['routes']['0'].segments['0'].steps,
            sPointName: sname,
            ePointName: ename,
            time: timedist['time'],
            distance: timedist['distance']
          };
          this.RouteListData.push(routeData);
          console.log('zwaracam trasę ', routeData);
          if (isLast === 1) {
            this.getLastRoute();
          } else if (isLast === 2) {
            this.RouteListData.sort((a, b) => a.route_number.toString().localeCompare(b.route_number.toString()));
            console.log(this.RouteListData);
            this.loading = false;
            this.isData = true;
          }

        },
        error => {
          this.responseErrorCatch(error);
        }
      );
  }

  returnRoadType(actualGeometryName) {
    let pref;
    switch (actualGeometryName) {
      case 'F': {
        pref = 'fastest';
        break;
      }
      case 'R': {
        pref = 'recommended';
        break;
      }
      case 'S': {
        pref = 'shortest';

        break;
      }
      default: {
        pref = 'recommended';

        break;
      }
    }
    return pref;
  }

  responseErrorCatch(error) {
    let errorMessage: any;
    this.loading = false;
    switch (error['error']['error']['message']) {
      case 'Request parameters exceed the server configuration limits. ' +
      'By dynamic weighting, the approximated distance of a route segment must not be greater than 300000.0 meters.': {
        errorMessage = 'Wybrane punkty są za daleko od siebie';
        break;
      }
      default: {
        errorMessage = error['error']['error']['message'];
        break;
      }
    }
    alert(errorMessage);
  }


  preparePointsData() {
    this.loading = true;
    for (let i = 0; i < this.actualHotelList.length; i++) {
      if (i === this.actualHotelList.length - 1) {
        this.getHotelInfo(this.actualHotelList[i].hotel_id, true, this.actualHotelList[i].hotel_number_trip);
      } else {
        this.getHotelInfo(this.actualHotelList[i].hotel_id, false, this.actualHotelList[i].hotel_number_trip);
      }
    }
  }

  getTimeAndDistance(resroutes) {
    let distance = parseFloat(resroutes['0'].summary['distance']) / 1000;
    let time = parseFloat(resroutes['0'].summary['duration']) / 60;
    distance = Math.round(distance);
    time = Math.round(time);
    return {
      distance: distance,
      time: time
    };
  }

  showRoute(road) {
    this.clearLayer();
    this.importRoute(road.geometry);
    this.setActualData(road.res_routes);
    this.getCenterBetween();
    this.showHotelData = false;
    this.showSteps = true;
    this.showCommentsRoad = true;
    this.roadId = road.road_id;
    this.getCommentsRoad(this.roadId);
  }

  getCenterBetween() {
    let center: any;
    center = geolib.getCenter([
      {
        latitude: this.actualGeometry[0][1],
        longitude: this.actualGeometry[0][0]
      },
      {
        latitude: this.actualGeometry[this.actualGeometry.length - 1][1],
        longitude: this.actualGeometry[this.actualGeometry.length - 1][0]
      }
    ]);
    // {latitude: "53.295895", longitude: "19.859769"}
    this.setCenter(center.longitude, center.latitude, 6);
  }

  setActualData(json) {
    this.actualGeometry = json['0'].geometry;
    this.steps = json['0'].segments['0'].steps;
    this.stepsArrayLength = json['0'].segments['0'].steps.length;
    this.actualStepNumber = 0;
    this.actualStep = this.steps[this.actualStepNumber];
    this.actualStep.instruction = this.returnInstruction(this.actualStep.type, this.actualStep.name, this.actualStep.distance);
  }

  returnInstruction(type, name, dist) {
    let instruction: string;
    switch (type) {
      case 0: {
        instruction = 'Za ' + dist + ' metrów skręć w lewo ';
        break;
      }
      case 1: {
        instruction = 'Za ' + dist + ' metrów skręć w prawo ';

        break;
      }
      case 2: {
        instruction = 'Za ' + dist + ' metrów skręć ostro w lewo ';
        break;
      }
      case 3: {
        instruction = 'Za ' + dist + ' metrów skręć ostro w prawo ';
        break;
      }
      case 4: {
        instruction = 'Za ' + dist + ' metrów skręć lekko w lewo ';
        break;
      }
      case 5: {
        instruction = 'Za ' + dist + ' metrów skręć lekko w prawo ';
        break;
      }
      case 6: {
        instruction = 'Jedź dalej prosto ';
        break;
      }
      case 7: {
        instruction = 'Na rondzie drugi zjazd ';
        break;
      }
      case 8: {
        instruction = 'Na rondzie trzeci zjazd ';
        break;
      }
      case 9: {
        instruction = 'Na rondzie czwarty zjazd ';
        break;
      }
      case 10: {
        instruction = 'Dotarłeś do celu ';
        break;
      }
      case 11: {
        instruction = 'Kieruj się na północ przez ' + dist + ' metrów';
        break;
      }
      case 12: {
        instruction = 'Trzymaj się lewej strony ';
        break;
      }
      case 13: {
        instruction = 'Trzymaj się prawej strony ';
        break;
      }
    }
    if (name !== '') {
      instruction = instruction + 'na ' + name;
    }
    return instruction;
  }

  nextStep() {
    if (this.actualStepNumber !== this.stepsArrayLength) {
      this.actualStepNumber++;
      this.actualStep = this.steps[this.actualStepNumber];
      this.actualStep.instruction = this.returnInstruction(this.actualStep.type, this.actualStep.name, this.actualStep.distance);
      this.setPositionMapPoint();
    }
  }

  previousStep() {
    if (this.actualStepNumber !== 0) {
      this.actualStepNumber--;
      this.actualStep = this.steps[this.actualStepNumber];
      this.actualStep.instruction = this.returnInstruction(this.actualStep.type, this.actualStep.name, this.actualStep.distance);
      this.setPositionMapPoint();
    }
  }

  setPositionMapPoint() {
    const num = this.actualStep['way_points'][0];
    this.setCenter(this.actualGeometry[num][0], this.actualGeometry[num][1], 17);
  }

  setCenter(lon, lat, zoom) {
    const view = this.map.getView();
    view.setCenter(ol.proj.fromLonLat([parseFloat(String(lon)), parseFloat(String(lat))]));
    view.setZoom(zoom);
  }

  importRoute(geometry) {
    const features = this.routeLayer.getSource().getFeatures();
    features.forEach((feature) => {
      this.routeLayer.getSource().removeFeature(feature);
    });
    const pointarray = [];
    for (let i = 0; i < geometry.length; i++) {
      pointarray.push([geometry[i][0], geometry[i][1]]);
    }
    const polyline = new ol.geom.LineString(pointarray);
    polyline.transform('EPSG:4326', 'EPSG:3857');

    this.routeLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: [new ol.Feature({
          geometry: polyline,
          name: 'Line'
        })]
      }),
      style: new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: '#333333',
          width: 3
        })
      })
    });
    this.routeLayer.setZIndex(5);
    this.map.addLayer(this.routeLayer);

  }

  clearLayer() {
    if (this.routeLayer.getSource() !== null) {
      const features = this.routeLayer.getSource().getFeatures();
      features.forEach((feature) => {
        this.routeLayer.getSource().removeFeature(feature);
      });
      this.markerSource.clear(true);
    }
  }



  // hotele
  showHotel(hotel) {
    this.showSteps = false;
    this.clearLayer();
    console.log(hotel);
    this.setCenter(hotel.lon, hotel.lat, 17);
    this.loading = true;
    // this.getHotelInfoSec(hotel.id);
    this.hotelInfo = hotel.hotelInfo;
    this.showCommentsRoad = false;
    this.hotelId = hotel['Id'];
    this.prepareDataToDisplay();

    this.getCommentsHotel(this.hotelId);
    this.hotelName = hotel.nameHotel;
    this.actualHotel = hotel;

    this.addMapPoint(hotel.lon, hotel.lat, '', 0);

  }
  getHotelInfoSec(id) {
    this.loading = true;

    const headers = new HttpHeaders()
      .set('Authorization', 'Bearer ' + this.apiservice.getSabreToken())
      .set('Content-Type', 'application/json');

    const body = {
      GetHotelDescriptiveInfoRQ: {
        HotelRefs: {
          HotelRef: [{
            HotelCode: id
          }]
        },
        DescriptiveInfoRef: {
          PropertyInfo: true,
          LocationInfo: true,
          Amenities: true,
          Descriptions: {
            Description: [{
              Type: 'Dining'
            }]
          },
          Airports: true,
          AcceptedCreditCards: true
        }
      }
    };

    this.http
      .post(this.apiservice.getSabreDescriptionUrl(), body, {headers: headers})
      .subscribe(
        res => {
          this.hotelInfo = res['GetHotelDescriptiveInfoRS'].HotelDescriptiveInfos['HotelDescriptiveInfo'];

          this.prepareDataToDisplay();

        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  addMapPoint(lon, lat, name, id) {
    this.iconFeature = new ol.Feature({
      geometry:
        new ol.geom.Point(
          ol.proj.transform([parseFloat(lon), parseFloat(lat)],
            'EPSG:4326',
            'EPSG:3857')
        ),
      name: name,
      id: id,
    });

    this.markerSource.addFeature(this.iconFeature);

  }

  prepareDataToDisplay() {
    try {
      this.floors = this.hotelInfo['0'].PropertyInfo['Floors'];
    } catch (e) {
      this.floors = 'Brak informacji';
    }

    try {
      this.rooms = this.hotelInfo['0'].PropertyInfo['Rooms'];
    } catch (e) {
      this.rooms = 'Brak informacji';
    }
    try {
      this.qualitty = this.hotelInfo['0'].PropertyInfo['PropertyQualityInfo'].PropertyQuality['0'].content;
    } catch (e) {
      this.qualitty = 'Brak informacji';
      console.log(e);
    }
    try {
      this.address = this.hotelInfo['0'].LocationInfo['Address'].AddressLine1;
    } catch (e) {
      this.address = 'Brak informacji';
      console.log(e);
    }
    try {
      this.phone = this.hotelInfo['0'].LocationInfo['Contact'].Phone;
    } catch (e) {
      this.phone = 'Brak informacji';
      console.log(e);
    }
    try {
      this.fax = this.hotelInfo['0'].LocationInfo['Contact'].Fax;
    } catch (e) {
      this.fax = 'Brak informacji';
      console.log(e);
    }
    let string = '';

    try {
      this.amenity = this.hotelInfo['0'].Amenities['Amenity'];
      for (const am of this.amenity) {
        string = string + am.Description + ',';
      }
    } catch (e) {
      this.translationText = 'Brak informacji';
      console.log(e);
    }
    if (this.translationText === 'Brak informacji') {
      string = 'Brak informacji';
    }

    try {
      this.creditcard = this.hotelInfo['0'].AcceptedCreditCards['CreditCard'];
    } catch (e) {
      this.creditcard = 'Brak informacji';
      console.log(e);
    }

    try {
      this.description = this.hotelInfo['0'].Descriptions['Description'][0].Text['content'];

    } catch (e) {
      this.description = 'Brak informacji';
      console.log(e);
    }

    this.newTranslate(string, 0);
    this.newTranslate(this.qualitty, 1);

    this.newTranslate(this.description, 2);

  }

  creditCardImg(name) {
    let url = '';
    switch (name) {
      case 'AMERICAN EXPRESS': {
        url = '../../assets/images/americanexpress.svg';
        break;
      }
      case 'MASTERCARD': {
        url = '../../assets/images/mastercard.svg';
        break;
      }
      case 'DINERS CLUB CARD': {
        url = '../../assets/images/dinersclub.svg';
        break;
      }
      case 'JCB CREDIT CARD': {
        url = '../../assets/images/jcb.svg';
        break;
      }
      case 'VISA': {
        url = '../../assets/images/visa.svg';
        break;
      }
      default: {
        break;
      }
    }
    return url;
  }

  newTranslate(text, num) {
    const url = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');

    const key = 'trnsl.1.1.20190126T164009Z.975ec213deedc2d3.7b16f5fe6ca9a9521c03c0467d2c22681c9aaecd';

    const body = 'key=' + key + '&text=' + text + '&lang=pl';

    this.http
      .post(url, body, {headers: headers})
      .subscribe(
        res => {
          if (num === 2) {
            this.translationTextDescr = res['text'].toString().toLocaleLowerCase();
            // this.translationTextDescr = this.translationTextDescr.toLowerCase();
            this.loading = false;
            this.showHotelData = true;
          } else if (num === 1) {
            this.qualitty = res['text'];
          } else {
            this.translationText = res['text'];
          }
        },
        error => {
          console.log(error);
        }
      );

  }

  sortByNameAZ() {
    this.tripList.sort((a, b) => a.name.localeCompare(b.name));
    // this.sortTooltipMessage = 'Sortuj od Z do A.';
  }

  sortByNameZA() {
    this.tripList.sort((a, b) => a.name.localeCompare(b.name));
    // this.sortTooltipMessage = 'Sortuj od A do Z.';
    this.tripList.reverse();
  }

  sortByAddDateO() {
    this.tripList.sort(
      (a, b) => a.created_date.substr(0, 16).replace('T', ' ').localeCompare(b.created_date.substr(0, 16).replace('T', ' ')));

  }

  sortByAddDateY() {
    this.tripList.sort(
      (a, b) => a.created_date.substr(0, 16).replace('T', ' ').localeCompare(b.created_date.substr(0, 16).replace('T', ' ')));
    this.tripList.reverse();
  }

  sortByUsageDateO() {
    this.tripList.sort(
      (a, b) => a.usage_date.substr(0, 16).replace('T', ' ').localeCompare(b.usage_date.substr(0, 16).replace('T', ' ')));

  }

  sortByUsageDateN() {
    this.tripList.sort(
      (a, b) => a.usage_date.substr(0, 16).replace('T', ' ').localeCompare(b.usage_date.substr(0, 16).replace('T', ' ')));
    this.tripList.reverse();
  }
  tabChange(event) {
    console.log(event.tab.textLabel);
    switch (event.tab.textLabel) {
      case 'Wszystkie': {
        this.tripList = this.tripListHelper;
        break;
      }
      case 'Archiwalne': {
        this.tripList = this.oldObject;
        break;
      }
      case 'Aktywne': {
        this.tripList = this.activeObject;
        console.log(this.activeObject);
        break;
      }
      default: {
        this.tripList = this.tripListHelper;
        break;
      }
    }
  }



  getCommentsRoad(id) {
    this.loading = true;

    this.http
      .get(this.apiservice.commentRoadList)
      .subscribe(
        res => {
          // this.comments
          const com = res['roads_comments'];
          // if (this.comments.length === 0) {
          //   this.comments = [];
          // }
          this.comments = [];
          for (const comment of com) {
            console.log(id);
            console.log(comment.road_id);
            if (comment.road_id === id) {
              this.comments.push(comment);
            }
          }

          console.log(res);
          this.loading = false;
        },
        error => {
          this.comments = [];
          this.loading = false;
        }
      );
  }

  getCommentsHotel(hotelId) {
    this.loading = true;

    this.http
      .get(this.apiservice.commentHotelList + hotelId)
      .subscribe(
        res => {
          this.comments = res['hotels_comments'];
          if (this.comments.length === 0) {
            this.comments = [];
          }
          console.log(this.comments);
          this.loading = false;
        },
        error => {
          this.comments = [];
          this.loading = false;
        }
      );
  }
}

class Point {
  constructor(
    public lon: number,
    public lat: number,
    public name: string,
  ) {
  }
}


