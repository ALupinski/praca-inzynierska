import {Component, HostBinding, OnInit, ViewChild} from '@angular/core';
import {ApiService} from '../apiconect/api.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {debounceTime, switchMap} from 'rxjs/operators';
import lodash from 'node_modules/lodash';
import {AppComponent} from '../app.component';
import {AuthorizationService} from '../apiconect/authorization.service';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {NamedialogComponent} from '../namedialog/namedialog.component';

declare var ol: any;


@Component({
  selector: 'app-chenk-hotel',
  templateUrl: './check-hotel.component.html',
  styleUrls: ['./check-hotel.component.sass']
})

export class CheckHotelComponent implements OnInit {
  hotelForm: FormControl;

  public loading = false;
  public showResultList = false;
  public showHotelData = false;
  public planing = false;
  private iconFeature: any;
  private markerSource = new ol.source.Vector();
  private markerStyle = new ol.style.Style({
    image: new ol.style.Icon(({
      anchorXUnits: 'fraction',
      scale: 0.05,
      anchorYUnits: 'pixels',
      opacity: 0.75,
      offset: [-100, -50],
      src: '../../assets/images/hotel.svg'
    }))
  });
  showSuggestion = false;
  public hotelList: any;
  public hotelInfo: any;
  map: any;
  private Point: Point;
  public form = new CheckHotelForm(null);
  private city: string;
  private code: string;
  private country: string;

  public floors: any;
  public rooms: any;
  public qualitty: any;
  public address: any;
  public phone: any;
  public fax: any;
  public amenity: any;
  public description: any;
  public creditcard: any;

  public translationText: any;
  public translationTextDescr: string;
  public hotelName: any;


  public errorMessage: string;
  public isError = false;

  cities: Sugestion [] = [
    ['Brak sugesti']
  ];
  public resultIsEmpty = false;
  private translateCity: string;
  private actualHotel: any;
  public comments: any;
  public hotelId: any;

  constructor(
    private dialog: MatDialog,
    private apiservice: ApiService,
    private authServicee: AuthorizationService,
    private http: HttpClient, private navBar: AppComponent) {
    this.hotelForm = new FormControl();
    this.hotelForm
      .valueChanges
      .pipe(
        switchMap(value => this.cities)
      );
  }

  ngOnInit() {
    this.map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        }),
        new ol.layer.Vector({
          source: this.markerSource,
          style: this.markerStyle,
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([20, 50]),
        zoom: 4.6,
        projection: 'EPSG:3857',
      }),
    });

    setTimeout(() => {
      this.map.updateSize();
    }, 100);
  }

  typed(event) {
    const query = event + '&format=json&addressdetails=1';
    this.http.get(this.apiservice.API_NOMINATIM + query).subscribe(
      res => {
        this.cities = [];
        for (let i = 0; i < 10; i++) {
          try {
            this.cities.push(
              [res[i].display_name,
                res[i].lon,
                res[i].lat,
                res[i].address.city,
                res[i].address.country_code,
                res[i].address.country]);
          } catch (e) {
          }
        }
        this.showSuggestion = true;
      },
      error => {
        console.log(error);
      }
    );
  }

  selectSuggest(item) {
    this.isError = false;
    if (item === undefined || item === null) {
      this.errorMessage = 'Nie dodano miasta, spróbuj ponownie';
      this.isError = true;
      return 0;
    }
    this.showSuggestion = false;
    this.Point = new Point(item['1'], item['2'], item['0']);
    this.form.city = item['0'];
    this.city = item['3'];
    this.code = item['4'];
    this.country = item['5'];
    this.setCenter(this.Point.lon, this.Point.lat, 10);
    this.loading = true;
    this.newTranslateCity(this.city);

  }

  private filterHotelList(city) {
    const newList = [];
    city = this.replacePolishChar(city);
    console.log(this.translateCity);
    for (const ob of this.hotelList) {
      if ((ob['City'] === city) || (ob['City'] === this.translateCity)) {
        newList.push(ob);
      }
    }
    console.log(this.hotelList);
    if (newList.length === 0) {
      this.resultIsEmpty = true;
    } else {
      this.resultIsEmpty = false;
      this.hotelList = newList;
    }
    console.log(newList);
  }

  submit() {
    this.sendPostRequest(this.city, this.code, this.country);
  }

  sendPostRequest(city, countryCode, country) {
    this.loading = true;

    const headers = new HttpHeaders()
      .set('Authorization', 'Bearer ' + this.apiservice.getSabreToken())
      .set('Content-Type', 'application/json');

    const body = {
      GeoSearchRQ: {
        version: '1',
        GeoRef: {
          Category: 'HOTEL',
          Radius: 20.0,
          UOM: 'MI',
          MaxResults: 300,
          OffSet: 1,
          AddressRef: {
            City: city,
            County: country,
            CountryCode: countryCode
          }
        }
      }
    };

    this.http
      .post(this.apiservice.getSabreUrl(), body, {headers: headers})
      .subscribe(
        res => {
          this.hotelList = res['GeoSearchRS'].GeoSearchResults['GeoSearchResult'];
          this.loading = false;
          this.filterHotelList(city);
          this.showResultList = true;
          if (!this.resultIsEmpty) {
            this.setHotelPoints();
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );


  }

  getHotelImages(id) {
    this.loading = true;

    const headers = new HttpHeaders()
      .set('Authorization', 'Bearer ' + this.apiservice.getSabreToken())
      .set('Content-Type', 'application/json');

    const body = {
      GetHotelImageRQ: {}
    };

    this.http
      .post('https://api-crt.cert.havail.sabre.com/v1.0.0/definitions/com.sabre.services.hotel.descriptiveinfo.v1.Amenities', body, {headers: headers})
      .subscribe(
        res => {
          this.loading = false;
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );


  }

  replacePolishChar(string) {
    return string.replace(/ą/g, 'a').replace(/Ą/g, 'A')
      .replace(/ć/g, 'c').replace(/Ć/g, 'C')
      .replace(/ę/g, 'e').replace(/Ę/g, 'E')
      .replace(/ł/g, 'l').replace(/Ł/g, 'L')
      .replace(/ń/g, 'n').replace(/Ń/g, 'N')
      .replace(/ó/g, 'o').replace(/Ó/g, 'O')
      .replace(/ś/g, 's').replace(/Ś/g, 'S')
      .replace(/ż/g, 'z').replace(/Ż/g, 'Z')
      .replace(/ź/g, 'z').replace(/Ź/g, 'Z');
  }

  getHotelInfo(id) {
    this.loading = true;

    const headers = new HttpHeaders()
      .set('Authorization', 'Bearer ' + this.apiservice.getSabreToken())
      .set('Content-Type', 'application/json');

    const body = {
      GetHotelDescriptiveInfoRQ: {
        HotelRefs: {
          HotelRef: [{
            HotelCode: id
          }]
        },
        DescriptiveInfoRef: {
          PropertyInfo: true,
          LocationInfo: true,
          Amenities: true,
          Descriptions: {
            Description: [{
              Type: 'Dining'
            }]
          },
          Airports: true,
          AcceptedCreditCards: true
        }
      }
    };

    this.http
      .post(this.apiservice.getSabreDescriptionUrl(), body, {headers: headers})
      .subscribe(
        res => {
          this.hotelInfo = res['GetHotelDescriptiveInfoRS'].HotelDescriptiveInfos['HotelDescriptiveInfo'];

          this.prepareDataToDisplay();
          this.getComments(id);

        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  setCenter(lon, lat, zoom) {
    const view = this.map.getView();
    view.setCenter(ol.proj.fromLonLat([parseFloat(String(lon)), parseFloat(String(lat))]));
    view.setZoom(zoom);
  }

  setHotelPoints() {
    for (const ob of this.hotelList) {
      this.addMapPoint(ob['Longitude'], ob['Latitude'], ob['Name'], ob['Id']);
    }
  }

  addMapPoint(lon, lat, name, id) {
    this.iconFeature = new ol.Feature({
      geometry:
        new ol.geom.Point(
          ol.proj.transform([parseFloat(lon), parseFloat(lat)],
            'EPSG:4326',
            'EPSG:3857')
        ),
      name: name,
      id: id,
    });

    this.markerSource.addFeature(this.iconFeature);

  }

  showHotel(hotel) {
    this.setCenter(hotel['Longitude'], hotel['Latitude'], 17);
    this.loading = true;
    this.getHotelInfo(hotel['Id']);
    this.hotelId = hotel['Id'];
    this.hotelName = hotel.Name;
    this.actualHotel = hotel;
  }

  prepareDataToDisplay() {
    try {
      this.floors = this.hotelInfo['0'].PropertyInfo['Floors'];
    } catch (e) {
      this.floors = 'Brak informacji';
    }

    try {
      this.rooms = this.hotelInfo['0'].PropertyInfo['Rooms'];
    } catch (e) {
      this.rooms = 'Brak informacji';
    }
    try {
      this.qualitty = this.hotelInfo['0'].PropertyInfo['PropertyQualityInfo'].PropertyQuality['0'].content;
    } catch (e) {
      this.qualitty = 'Brak informacji';
      console.log(e);
    }
    try {
      this.address = this.hotelInfo['0'].LocationInfo['Address'].AddressLine1;
    } catch (e) {
      this.address = 'Brak informacji';
      console.log(e);
    }
    try {
      this.phone = this.hotelInfo['0'].LocationInfo['Contact'].Phone;
    } catch (e) {
      this.phone = 'Brak informacji';
      console.log(e);
    }
    try {
      this.fax = this.hotelInfo['0'].LocationInfo['Contact'].Fax;
    } catch (e) {
      this.fax = 'Brak informacji';
      console.log(e);
    }
    let string = '';

    try {
      this.amenity = this.hotelInfo['0'].Amenities['Amenity'];
      for (const am of this.amenity) {
        string = string + am.Description + ',';
      }
    } catch (e) {
      this.translationText = 'Brak informacji';
      console.log(e);
    }
    if (this.translationText === 'Brak informacji') {
      string = 'Brak informacji';
    }

    try {
      this.creditcard = this.hotelInfo['0'].AcceptedCreditCards['CreditCard'];
    } catch (e) {
      this.creditcard = 'Brak informacji';
      console.log(e);
    }

    try {
      this.description = this.hotelInfo['0'].Descriptions['Description'][0].Text['content'];

    } catch (e) {
      this.description = 'Brak informacji';
      console.log(e);
    }
    // if (this.creditcard !== 'Brak informacji') {
    //   for (const card of this.creditcard) {
    //     card.content = this.creditCardImg(card.content);
    //   }
    // }
    this.newTranslate(string, 0);
    this.newTranslate(this.qualitty, 1);

    this.newTranslate(this.description, 2);

  }

  creditCardImg(name) {
    let url = '';
    switch (name) {
      case 'AMERICAN EXPRESS': {
        url = '../../assets/images/americanexpress.svg';
        break;
      }
      case 'MASTERCARD': {
        url = '../../assets/images/mastercard.svg';
        break;
      }
      case 'DINERS CLUB CARD': {
        url = '../../assets/images/dinersclub.svg';
        break;
      }
      case 'JCB CREDIT CARD': {
        url = '../../assets/images/jcb.svg';
        break;
      }
      case 'VISA': {
        url = '../../assets/images/visa.svg';
        break;
      }
      default: {
        break;
      }
    }
    return url;
  }

  newTranslate(text, num) {
    const url = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');

    const key = 'trnsl.1.1.20190126T164009Z.975ec213deedc2d3.7b16f5fe6ca9a9521c03c0467d2c22681c9aaecd';

    const body = 'key=' + key + '&text=' + text + '&lang=pl';

    this.http
      .post(url, body, {headers: headers})
      .subscribe(
        res => {
          if (num === 2) {
            this.translationTextDescr = res['text'].toString().toLocaleLowerCase();
            // this.translationTextDescr = this.translationTextDescr.toLowerCase();
            this.loading = false;
            this.showHotelData = true;
          } else if (num === 1) {
            this.qualitty = res['text'];
          } else {
            this.translationText = res['text'];
          }
        },
        error => {
          console.log(error);
        }
      );

  }

  newTranslateCity(text) {
    const url = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');

    const key = 'trnsl.1.1.20190126T164009Z.975ec213deedc2d3.7b16f5fe6ca9a9521c03c0467d2c22681c9aaecd';

    const body = 'key=' + key + '&text=' + text + '&lang=en';

    this.http
      .post(url, body, {headers: headers})
      .subscribe(
        res => {
          this.translateCity = res['text'].toString();
          this.loading = false;
        },
        error => {
          console.log(error);
        }
      );

  }

  saveHotelData() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.position = {
      left: '35%',
      right: '35%',
      top: '10%'
    };
    dialogConfig.width = '25%';
    let dataName;
    let date;
    const dialogRef = this.dialog.open(NamedialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        if (data === null) {
          return;
        }
        dataName = data.name;
        date = data.date;
        const hotel = {
          id: this.actualHotel['Id'],
          name: this.hotelName,
          lon: this.actualHotel['Longitude'],
          lat: this.actualHotel['Latitude'],
          floors: this.floors,
          rooms: this.rooms,
          quality: '',
          address: this.address,
          phone: this.phone,
          fax: this.fax,
          amenity: '',
          description: '',
          usage_date: date
        };
        console.log(this.authServicee.getIsAuthorize());
        if (!this.authServicee.getIsAuthorize()) {
          alert('Zaloguj sie żeby zapisać hotel');
          return;
        } else {
          this.saveHotelToBase(hotel);
        }
      }
    );
  }

  saveHotelToBase(hotel) {
    this.loading = true;

    const headers = new HttpHeaders()
      .set('Authorization', 'Token ' + this.authServicee.getToken());


    this.http
      .post(this.apiservice.hotelcreateUrl, hotel, {headers: headers})
      .subscribe(
        res => {
          this.loading = false;
          alert('Pomyślnie dodano hotel ');
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  getComments(hotelId) {
    this.loading = true;

    this.http
      .get(this.apiservice.commentHotelList + hotelId)
      .subscribe(
        res => {
          this.comments = res['hotels_comments'];
          if (this.comments.length === 0) {
            this.comments = [];
          }
          console.log(this.comments);
          this.loading = false;
        },
        error => {
          this.comments = [];
          this.loading = false;
        }
      );
  }
}

class Point {
  constructor(
    public lon: number,
    public lat: number,
    public name: string,
  ) {
  }
}

class CheckHotelForm {
  constructor(
    public city: string,
  ) {
  }
}

class Sugestion {
  constructor(
    text: string,
    lon: number,
    lat: number,
    city: string,
    country_code: string,
    country: string
  ) {

  }
}


