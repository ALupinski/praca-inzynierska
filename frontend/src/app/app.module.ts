import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {AppComponent} from './app.component';
import {RegisterComponent} from './register/register.component';
import {HomeComponent} from './home/home.component';
import {CheckHotelComponent} from './check-hotel/check-hotel.component';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {MatDialogModule, MatNativeDateModule, MatTabsModule} from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CheckRoadComponent} from './check-road/check-road.component';
import {PlanTripComponent} from './plan-trip/plan-trip.component';
import {UserComponent} from './user/user.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ApiService} from './apiconect/api.service';
import {AccountDetailsComponent} from './user/account-details/account-details.component';
import {MyReservedHotelsComponent} from './user/my-reserved-hotels/my-reserved-hotels.component';
import {MyRoadsComponent} from './user/my-roads/my-roads.component';
import {MyTripsComponent} from './user/my-trips/my-trips.component';
import {SelectModule} from 'ng2-select';
import {MomentModule} from 'ngx-moment';
import {CoordinatesModule} from 'angular-coordinates';
import 'rxjs-compat/Observable';
import {NgxLoadingModule} from 'ngx-loading';
import {NewTripFormComponent} from './new-trip-form/new-trip-form.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { NamedialogComponent } from './namedialog/namedialog.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatTooltipModule} from '@angular/material/tooltip';
import { CommentsListComponent } from './comments-list/comments-list.component';
import {StarRatingModule} from 'angular-star-rating';
import {DatePipe} from '@angular/common';


const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'hotel-list', component: CheckHotelComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'road-list', component: CheckRoadComponent},
  {path: 'plan-trip', component: PlanTripComponent},
  {
    path: 'user', component: UserComponent,
    children: [
      {path: '', component: AccountDetailsComponent},
      {path: 'my-hotels', component: MyReservedHotelsComponent},
      {path: 'my-roads', component: MyRoadsComponent},
      {path: 'my-trips', component: MyTripsComponent}
    ]
  },
];

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    HomeComponent,
    CheckHotelComponent,
    LoginComponent,
    CheckRoadComponent,
    PlanTripComponent,
    UserComponent,
    AccountDetailsComponent,
    MyReservedHotelsComponent,
    MyRoadsComponent,
    MyTripsComponent,
    NewTripFormComponent,
    NamedialogComponent,
    CommentsListComponent,
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true} // <-- debugging purposes only
    ),
    BrowserModule,
    HttpModule,
    MatDialogModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    SelectModule,
    MomentModule,
    MatIconModule,
    NgxLoadingModule.forRoot({}),
    CoordinatesModule,
    MatSlideToggleModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTooltipModule,
    MatTabsModule
  ],
  providers: [
    ApiService,
    DatePipe
  ],

  bootstrap: [AppComponent],
  entryComponents: [
    HomeComponent,
    NewTripFormComponent,
    NamedialogComponent
  ]

})
export class AppModule {
}

// ToastModule.forRoot()
