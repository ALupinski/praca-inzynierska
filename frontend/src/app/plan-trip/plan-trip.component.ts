import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {NewTripFormComponent} from '../new-trip-form/new-trip-form.component';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ApiService} from '../apiconect/api.service';
import * as geolib from 'geolib';
import {AuthorizationService} from '../apiconect/authorization.service';
import {NamedialogComponent} from '../namedialog/namedialog.component';

declare var ol: any;

@Component({
  selector: 'app-plan-trip',
  templateUrl: './plan-trip.component.html',
  styleUrls: ['./plan-trip.component.sass']
})
export class PlanTripComponent implements OnInit {
  map: any;
  returnedData: any;
  public planing = true;
  private markerStyle = new ol.style.Style({
    image: new ol.style.Circle({
      radius: 7,
      stroke: new ol.style.Stroke({
        color: '#333333'
      }),
      fill: new ol.style.Fill({
        color: '#333333' // attribute colour
      })
    }),
  });
  private markerStyleHotel = new ol.style.Style({
    image: new ol.style.Icon(({
      positioning: 'bottom-center',
      anchorXUnits: 'fraction',
      scale: 0.05,
      anchorYUnits: 'pixels',
      src: '../../assets/images/hotel.svg'
    }))
  });

  private markerSource = new ol.source.Vector();

  public findHotelMode: boolean;

  // znajdz hotel

  public findRoadMode: boolean;
  public loading: boolean;
  private hotelInfo: any;
  public hotelList: any;
  public showResultList: boolean;
  private iconFeature: any;
  public hotelName: any;
  public floors: any;
  public rooms: any;
  public qualitty: any;
  public address: any;
  public phone: any;
  public fax: any;
  public amenity: any;
  public translationText: string;
  public creditcard: any;
  public description: any;
  public translationTextDescr: string;
  public showHotelData: boolean;
  public actualCity: any;
  public actualCityCounter: number;
  private accommodationCityList: any;
  private savedHotelList = [];
  public isLast = false;
  public resultIsEmpty = false;

  public actualHotelId: any;
  private routeLayer = new ol.layer.Vector();
  private actualHotelLon: any;
  private actualHotelLat: any;

  // znajdz drogę
  public sPoint = new Point(0, 0, '');
  public ePoint = new Point(0, 0, '');
  private query: string;
  public fastTime: any;
  public fastDistance: any;
  public shortTime: any;
  public shortDistance: any;
  public recomendTime: any;
  public recomendDistance: any;
  private requestedRuteFastest: any;
  private requestedRuteShortest: any;
  private requestedRuteRecomended: any;
  public showRouteResoult = false;
  public showStepsR = false;
  public showStepsF = false;
  public showStepsS = false;
  private actualStepNumber: number;
  private steps: any;
  private stepsArrayLength: number;
  actualStep: any;
  private actualGeometry: any;
  public isToken = false;
  public message: string;
  private allRouteNumber: number;
  private actualRouteNumber: number;
  private routeList = [];
  private actualGeometryName: any;
  private createdTripId: any;
  private StartPoinTrip: Point;
  private EndPoinTrip: Point;

  public comments: any[];
  public showCommentsRoad = false;
  public roadId: any;
  public hotelId: any;


  constructor(private dialog: MatDialog,
              private apiservice: ApiService,
              private http: HttpClient,
              private authService: AuthorizationService) {
  }

  clearLayer() {
    if (this.routeLayer.getSource() !== null) {
      const features = this.routeLayer.getSource().getFeatures();
      features.forEach((feature) => {
        this.routeLayer.getSource().removeFeature(feature);
      });
      this.markerSource.clear(true);
    }
  }

  ngOnInit() {
    this.map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        }),
        new ol.layer.Vector({
          source: this.markerSource,
          style: this.markerStyleHotel,
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([20, 50]),
        zoom: 4.6,
        projection: 'EPSG:3857',
      }),
    });

    setTimeout(() => {
      this.map.updateSize();
    }, 100);
    this.showTripSettings();
  }

  showTripSettings() {
    this.planing = true;
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = false;
    dialogConfig.position = {
      left: '35%',
      right: '35%',
      top: '10%'
    };
    dialogConfig.width = '25%';

    const dialogRef = this.dialog.open(NewTripFormComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data === null) {
          this.planing = false;
          return;
        }
        console.log(this.returnedData);
        this.returnedData = data;
        this.findHotelMode = true;
        this.StartPoinTrip = new Point(
          this.returnedData['startPoint'].lon,
          this.returnedData['startPoint'].lat,
          this.returnedData['startPoint'].name);
        this.EndPoinTrip = new Point(
          this.returnedData['endPoint'].lon,
          this.returnedData['endPoint'].lat,
          this.returnedData['endPoint'].name);

        this.accommodationCityList = this.returnedData['cityTab'];
        console.log(this.accommodationCityList);
        this.actualCityCounter = 0;
        this.loading = true;
        this.actualCity = this.accommodationCityList[this.actualCityCounter];
        this.setCenter(this.actualCity[1], this.actualCity[2], 10);
        this.sendPostRequest(this.actualCity[3], this.actualCity[4], this.actualCity[5]);
        if (this.actualCityCounter === this.accommodationCityList.length - 1) {
          this.isLast = true;
        }
        this.createSavedHotelList();
      }
    );


  }

  nextCity() {
    if (this.actualCityCounter !== this.accommodationCityList.length) {
      this.actualCityCounter++;
      this.hotelList = null;
      this.actualCity = this.accommodationCityList[this.actualCityCounter];
      this.setCenter(this.actualCity[1], this.actualCity[2], 10);
      this.sendPostRequest(this.actualCity[3], this.actualCity[4], this.actualCity[5]);
      this.showHotelData = false;
      this.clearLayer();
      if (this.actualCityCounter === this.accommodationCityList.length - 1) {
        this.isLast = true;
      }

    }
  }

  prevCity() {
    if (this.actualCityCounter !== 0) {
      this.actualCityCounter--;
      this.hotelList = null;
      this.actualCity = this.accommodationCityList[this.actualCityCounter];
      this.setCenter(this.actualCity[1], this.actualCity[2], 10);
      this.sendPostRequest(this.actualCity[3], this.actualCity[4], this.actualCity[5]);
      this.showHotelData = false;
      this.clearLayer();

      this.markerSource = new ol.source.Vector();

    }
  }

  createSavedHotelList() {
    for (let i = 0; i < this.accommodationCityList.length; i++) {
      this.savedHotelList.push([this.accommodationCityList[i][0], -1]);
    }
  }

  saveHotelToList(id) {
    this.savedHotelList[this.actualCityCounter][1] = id;
    // zapis nowych kordow
    this.accommodationCityList[this.actualCityCounter]['1'] = this.actualHotelLon; // lon
    this.accommodationCityList[this.actualCityCounter]['2'] = this.actualHotelLat; // lat
    alert('Pomyślno zapisano hotel');
  }

  // znajdz hotel
  sendPostRequest(city, countryCode, country) {
    this.loading = true;

    const headers = new HttpHeaders()
      .set('Authorization', 'Bearer ' + this.apiservice.getSabreToken())
      .set('Content-Type', 'application/json');

    const body = {
      GeoSearchRQ: {
        version: '1',
        GeoRef: {
          Category: 'HOTEL',
          Radius: 20.0,
          UOM: 'MI',
          MaxResults: 300,
          OffSet: 1,
          AddressRef: {
            City: city,
            County: country,
            CountryCode: countryCode
          }
        }
      }
    };

    this.http
      .post(this.apiservice.getSabreUrl(), body, {headers: headers})
      .subscribe(
        res => {
          this.hotelList = res['GeoSearchRS'].GeoSearchResults['GeoSearchResult'];
          this.loading = false;
          this.showResultList = true;
          this.filterHotelList(city);
          if (!this.resultIsEmpty) {
            this.setHotelPoints();
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );


  }

  getHotelInfo(id) {
    this.loading = true;

    const headers = new HttpHeaders()
      .set('Authorization', 'Bearer ' + this.apiservice.getSabreToken())
      .set('Content-Type', 'application/json');

    const body = {
      GetHotelDescriptiveInfoRQ: {
        HotelRefs: {
          HotelRef: [{
            HotelCode: id
          }]
        },
        DescriptiveInfoRef: {
          PropertyInfo: true,
          LocationInfo: true,
          Amenities: true,
          Descriptions: {
            Description: [{
              Type: 'Dining'
            }]
          },
          Airports: true,
          AcceptedCreditCards: true
        }
      }
    };

    this.http
      .post(this.apiservice.getSabreDescriptionUrl(), body, {headers: headers})
      .subscribe(
        res => {
          console.log(res);
          this.hotelInfo = res['GetHotelDescriptiveInfoRS'].HotelDescriptiveInfos['HotelDescriptiveInfo'];

          this.prepareDataToDisplay();
          this.getCommentsHotel(id);

        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  setCenter(lon, lat, zoom) {
    const view = this.map.getView();
    view.setCenter(ol.proj.fromLonLat([parseFloat(String(lon)), parseFloat(String(lat))]));
    view.setZoom(zoom);
  }

  setHotelPoints() {
    for (const ob of this.hotelList) {
      this.addMapPoint(ob['Longitude'], ob['Latitude'], ob['Id']);
    }
  }

  addMapPoint(lon, lat, id) {
    this.iconFeature = new ol.Feature({
      geometry:
        new ol.geom.Point(
          ol.proj.transform([parseFloat(lon), parseFloat(lat)],
            'EPSG:4326',
            'EPSG:3857')
        ),
      id: id,
    });

    this.markerSource.addFeature(this.iconFeature);

  }

  showHotel(hotel) {
    this.setCenter(hotel['Longitude'], hotel['Latitude'], 17);
    this.loading = true;
    this.actualHotelLon = hotel['Longitude'];
    this.actualHotelLat = hotel['Latitude'];
    this.actualHotelId = hotel['Id'];
    this.getHotelInfo(hotel['Id']);
    this.hotelId = hotel['Id'];
    this.hotelName = hotel.Name;

  }

  prepareDataToDisplay() {
    try {
      this.floors = this.hotelInfo['0'].PropertyInfo['Floors'];
    } catch (e) {
      this.floors = 'Brak informacji';
    }

    try {
      this.rooms = this.hotelInfo['0'].PropertyInfo['Rooms'];
    } catch (e) {
      this.rooms = 'Brak informacji';
    }
    try {
      this.qualitty = this.hotelInfo['0'].PropertyInfo['PropertyQualityInfo'].PropertyQuality['0'].content;
    } catch (e) {
      this.qualitty = 'Brak informacji';
      console.log(e);
    }
    try {
      this.address = this.hotelInfo['0'].LocationInfo['Address'].AddressLine1;
    } catch (e) {
      this.address = 'Brak informacji';
      console.log(e);
    }
    try {
      this.phone = this.hotelInfo['0'].LocationInfo['Contact'].Phone;
    } catch (e) {
      this.phone = 'Brak informacji';
      console.log(e);
    }
    try {
      this.fax = this.hotelInfo['0'].LocationInfo['Contact'].Fax;
    } catch (e) {
      this.fax = 'Brak informacji';
      console.log(e);
    }
    let string = '';

    try {
      this.amenity = this.hotelInfo['0'].Amenities['Amenity'];
      for (const am of this.amenity) {
        string = string + am.Description + ',';
      }
    } catch (e) {
      this.translationText = 'Brak informacji';
      console.log(e);
    }
    if (this.translationText === 'Brak informacji') {
      string = 'Brak informacji';
    }

    try {
      this.creditcard = this.hotelInfo['0'].AcceptedCreditCards['CreditCard'];
    } catch (e) {
      this.creditcard = 'Brak informacji';
      console.log(e);
    }

    try {
      this.description = this.hotelInfo['0'].Descriptions['Description'][0].Text['content'];

    } catch (e) {
      this.description = 'Brak informacji';
      console.log(e);
    }
    this.newTranslate(string, 0);
    this.newTranslate(this.qualitty, 1);

    this.newTranslate(this.description, 2);

  }

  creditCardImg(name) {
    let url = '';
    switch (name) {
      case 'AMERICAN EXPRESS': {
        url = '../../assets/images/americanexpress.svg';
        break;
      }
      case 'MASTERCARD': {
        url = '../../assets/images/mastercard.svg';
        break;
      }
      case 'DINERS CLUB CARD': {
        url = '../../assets/images/dinersclub.svg';
        break;
      }
      case 'JCB CREDIT CARD': {
        url = '../../assets/images/jcb.svg';
        break;
      }
      case 'VISA': {
        url = '../../assets/images/visa.svg';
        break;
      }
      default: {
        break;
      }
    }
    return url;
  }

  newTranslate(text, num) {
    const url = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');

    const key = 'trnsl.1.1.20190126T164009Z.975ec213deedc2d3.7b16f5fe6ca9a9521c03c0467d2c22681c9aaecd';

    const body = 'key=' + key + '&text=' + text + '&lang=pl';

    this.http
      .post(url, body, {headers: headers})
      .subscribe(
        res => {
          if (num === 2) {
            this.translationTextDescr = res['text'].toString().toLocaleLowerCase();
            // this.translationTextDescr = this.translationTextDescr.toLowerCase();
            this.loading = false;
            this.showHotelData = true;
          } else if (num === 1) {
            this.qualitty = res['text'];
          } else {
            this.translationText = res['text'];
          }
        },
        error => {
          console.log(error);
        }
      );

  }

  private filterHotelList(city) {
    const newList = [];
    city = this.replacePolishChar(city);
    for (const ob of this.hotelList) {
      if (ob['City'] === city) {
        newList.push(ob);
      }
    }
    if (newList.length === 0) {
      this.resultIsEmpty = true;
    } else {
      this.resultIsEmpty = false;
      this.hotelList = newList;
    }
  }

  replacePolishChar(string) {
    return string.replace(/ą/g, 'a').replace(/Ą/g, 'A')
      .replace(/ć/g, 'c').replace(/Ć/g, 'C')
      .replace(/ę/g, 'e').replace(/Ę/g, 'E')
      .replace(/ł/g, 'l').replace(/Ł/g, 'L')
      .replace(/ń/g, 'n').replace(/Ń/g, 'N')
      .replace(/ó/g, 'o').replace(/Ó/g, 'O')
      .replace(/ś/g, 's').replace(/Ś/g, 'S')
      .replace(/ż/g, 'z').replace(/Ż/g, 'Z')
      .replace(/ź/g, 'z').replace(/Ź/g, 'Z');
  }

  // znajdz drogę
  goToRoadCheck() {
    this.clearLayer();
    this.showHotelData = false;
    this.isLast = false;
    this.findHotelMode = false;
    this.findRoadMode = true;
    this.allRouteNumber = this.accommodationCityList.length + 1;
    this.sPoint = new Point(
      this.returnedData['startPoint'].lon,
      this.returnedData['startPoint'].lat,
      this.returnedData['startPoint'].name);
    this.ePoint = new Point(
      this.accommodationCityList['0']['1'],
      this.accommodationCityList['0']['2'],
      this.accommodationCityList['0']['3']);
    this.getAllWays();
    this.actualRouteNumber = 0;
    this.getCenterBetween();
    this.setMarkers();
  }

  getCenterBetween() {
    let center: any;
    center = geolib.getCenter([
      {latitude: this.sPoint.lat, longitude: this.sPoint.lon},
      {latitude: this.ePoint.lat, longitude: this.ePoint.lon}
    ]);
    // {latitude: "53.295895", longitude: "19.859769"}
    this.setCenter(center.longitude, center.latitude, 6);
  }

  getTimeAndDistance() {
    this.fastDistance = parseFloat(this.requestedRuteFastest['0'].summary['distance']) / 1000;
    this.fastTime = parseFloat(this.requestedRuteFastest['0'].summary['duration']) / 60;
    this.shortDistance = parseFloat(this.requestedRuteShortest['0'].summary['distance']) / 1000;
    this.shortTime = parseFloat(this.requestedRuteShortest['0'].summary['duration']) / 60;
    this.recomendDistance = parseFloat(this.requestedRuteRecomended['0'].summary['distance']) / 1000;
    this.recomendTime = parseFloat(this.requestedRuteRecomended['0'].summary['duration']) / 60;
    this.fastDistance = Math.round(this.fastDistance);
    this.fastTime = Math.round(this.fastTime);
    this.shortDistance = Math.round(this.shortDistance);
    this.shortTime = Math.round(this.shortTime);
    this.recomendDistance = Math.round(this.recomendDistance);
    this.recomendTime = Math.round(this.recomendTime);

  }

  getAllWays() {
    this.getWayF();
    this.getWayS();
    this.getWayR();

  }

  setMarkers() {
    this.clearLayer();
    if (this.ePoint !== undefined && this.sPoint !== undefined) {
      this.addMapPoint(this.sPoint.lon, this.sPoint.lat, 0);
      this.addMapPoint(this.ePoint.lon, this.ePoint.lat, 1);
      this.lineDraw();
    } else {
      this.message = 'Podaj punkt startowy i docelowy';
    }
  }

  lineDraw() {
    const line_point = [];
    line_point.push(ol.proj.fromLonLat([parseFloat(String(this.sPoint.lon)), parseFloat(String(this.sPoint.lat))]));
    line_point.push(ol.proj.fromLonLat([parseFloat(String(this.ePoint.lon)), parseFloat(String(this.ePoint.lat))]));
    this.routeLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: [new ol.Feature({
          geometry: new ol.geom.LineString(line_point, 'XY'),
          name: 'Line'
        })]
      }),
      style: new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: '#333333',
          width: 4
        })
      })
    });
    this.routeLayer.setZIndex(5);
    this.map.addLayer(this.routeLayer);
  }

  getWayF() {
    this.loading = true;
    this.http.get(this.apiservice.getOpenRouteUrl(
      this.sPoint.lon,
      this.sPoint.lat,
      this.ePoint.lon,
      this.ePoint.lat,
      'fastest',
      'driving-car'))
      .debounceTime(400)
      .subscribe(
        res => {
          this.requestedRuteFastest = res['routes'];
        },
        error => {
          this.responseErrorCatch(error);
          console.log(error);
        }
      );
  }

  getWayR() {
    this.http.get(this.apiservice.getOpenRouteUrl(
      this.sPoint.lon,
      this.sPoint.lat,
      this.ePoint.lon,
      this.ePoint.lat,
      'recommended',
      'driving-hgv'))
      .debounceTime(400)
      .subscribe(
        res => {
          this.requestedRuteRecomended = res['routes'];
          this.loading = false;
          this.getTimeAndDistance();
          this.showRouteResoult = true;
        },
        error => {
          this.responseErrorCatch(error);
          console.log(error);
        }
      );
  }

  getWayS() {
    this.http.get(this.apiservice.getOpenRouteUrl(
      this.sPoint.lon,
      this.sPoint.lat,
      this.ePoint.lon,
      this.ePoint.lat,
      'shortest',
      'driving-car'))
      .debounceTime(400)
      .subscribe(
        res => {
          this.requestedRuteShortest = res['routes'];
        },
        error => {
          this.responseErrorCatch(error);
          console.log(error);
        }
      );
  }

  setActualData(json, name) {
    this.setShowFalse();
    this.actualGeometry = json['0'].geometry;
    this.actualGeometryName = name;
    this.steps = json['0'].segments['0'].steps;
    this.stepsArrayLength = json['0'].segments['0'].steps.length;
    this.actualStepNumber = 0;
    this.actualStep = this.steps[this.actualStepNumber];
    this.actualStep.instruction = this.returnInstruction(this.actualStep.type, this.actualStep.name, this.actualStep.distance);
  }

  setShowFalse() {
    this.showStepsR = false;
    this.showStepsS = false;
    this.showStepsF = false;

  }

  chooseRoute(number) {
    this.showCommentsRoad = true;
    this.setShowFalse();
    switch (number) {
      case 1: {
        this.setActualData(this.requestedRuteFastest, 'F');
        this.showStepsF = true;
        this.importRoute(this.actualGeometry);
        // this.setPositionMapPoint();
        break;
      }
      case 2: {
        console.log(this.requestedRuteShortest);
        this.setActualData(this.requestedRuteShortest, 'S');
        this.showStepsS = true;
        this.importRoute(this.actualGeometry);
        // this.setPositionMapPoint();
        break;
      }
      case 3: {
        this.setActualData(this.requestedRuteRecomended, 'R');
        this.showStepsR = true;
        this.importRoute(this.actualGeometry);
        // this.setPositionMapPoint();
        break;
      }
      default: {
        this.setActualData(this.requestedRuteRecomended, 'R');
        this.showStepsR = true;
        this.importRoute(this.actualGeometry);
        // this.setPositionMapPoint();
        break;
      }
    }
    this.roadId = this.sPoint.lon.toString() +
      this.sPoint.lat.toString() +
      this.ePoint.lon.toString() +
      this.ePoint.lat.toString() +
      this.actualGeometryName.toString();
    this.roadId = this.roadId.replace(/\./g, '');
    this.getCommentsRoad(this.roadId);
  }

  importRoute(geometry) {
    const features = this.routeLayer.getSource().getFeatures();
    features.forEach((feature) => {
      this.routeLayer.getSource().removeFeature(feature);
    });
    const pointarray = [];
    for (let i = 0; i < geometry.length; i++) {
      pointarray.push([geometry[i][0], geometry[i][1]]);
    }
    const polyline = new ol.geom.LineString(pointarray);
    polyline.transform('EPSG:4326', 'EPSG:3857');

    this.routeLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: [new ol.Feature({
          geometry: polyline,
          name: 'Line'
        })]
      }),
      style: new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: '#333333',
          width: 3
        })
      })
    });
    this.routeLayer.setZIndex(5);
    this.map.addLayer(this.routeLayer);

  }

  nextStep() {
    if (this.actualStepNumber !== this.stepsArrayLength) {
      this.actualStepNumber++;
      this.actualStep = this.steps[this.actualStepNumber];
      this.actualStep.instruction = this.returnInstruction(this.actualStep.type, this.actualStep.name, this.actualStep.distance);
      this.setPositionMapPoint();
    }
  }

  previousStep() {
    if (this.actualStepNumber !== 0) {
      this.actualStepNumber--;
      this.actualStep = this.steps[this.actualStepNumber];
      this.actualStep.instruction = this.returnInstruction(this.actualStep.type, this.actualStep.name, this.actualStep.distance);
      this.setPositionMapPoint();
    }
  }

  setPositionMapPoint() {
    const num = this.actualStep['way_points'][0];
    this.setCenter(this.actualGeometry[num][0], this.actualGeometry[num][1], 17);
  }

  responseErrorCatch(error) {
    let errorMessage: any;
    this.loading = false;
    switch (error['error']['error']['message']) {
      case 'Request parameters exceed the server configuration limits. ' +
      'By dynamic weighting, the approximated distance of a route segment must not be greater than 300000.0 meters.': {
        errorMessage = 'Wybrane punkty są za daleko od siebie';
        break;
      }
      default: {
        errorMessage = error['error']['error']['message'];
        break;
      }
    }
    alert(errorMessage);
  }

  returnInstruction(type, name, dist) {
    let instruction: string;
    switch (type) {
      case 0: {
        instruction = 'Za ' + dist + ' metrów skręć w lewo ';
        break;
      }
      case 1: {
        instruction = 'Za ' + dist + ' metrów skręć w prawo ';

        break;
      }
      case 2: {
        instruction = 'Za ' + dist + ' metrów skręć ostro w lewo ';
        break;
      }
      case 3: {
        instruction = 'Za ' + dist + ' metrów skręć ostro w prawo ';
        break;
      }
      case 4: {
        instruction = 'Za ' + dist + ' metrów skręć lekko w lewo ';
        break;
      }
      case 5: {
        instruction = 'Za ' + dist + ' metrów skręć lekko w prawo ';
        break;
      }
      case 6: {
        instruction = 'Jedź dalej prosto ';
        break;
      }
      case 7: {
        instruction = 'Na rondzie drugi zjazd ';
        break;
      }
      case 8: {
        instruction = 'Na rondzie trzeci zjazd ';
        break;
      }
      case 9: {
        instruction = 'Na rondzie czwarty zjazd ';
        break;
      }
      case 10: {
        instruction = 'Dotarłeś do celu ';
        break;
      }
      case 11: {
        instruction = 'Kieruj się na północ przez ' + dist + ' metrów';
        break;
      }
      case 12: {
        instruction = 'Trzymaj się lewej strony ';
        break;
      }
      case 13: {
        instruction = 'Trzymaj się prawej strony ';
        break;
      }
    }
    if (name !== '') {
      instruction = instruction + 'na ' + name;
    }
    return instruction;
  }

  nextRoad() {
    if (this.actualRouteNumber !== this.allRouteNumber - 1) {
      this.actualRouteNumber++;
    }
    console.log(this.actualRouteNumber, '  <=aktualny ', this.allRouteNumber, ' <= wszystkie ');
    if (this.actualRouteNumber !== this.allRouteNumber - 1) {
      this.sPoint = new Point(
        this.accommodationCityList[this.actualRouteNumber - 1]['1'],
        this.accommodationCityList[this.actualRouteNumber - 1]['2'],
        this.accommodationCityList[this.actualRouteNumber - 1]['3']);
      this.ePoint = new Point(
        this.accommodationCityList[this.actualRouteNumber]['1'],
        this.accommodationCityList[this.actualRouteNumber]['2'],
        this.accommodationCityList[this.actualRouteNumber]['3']);
    }
    if (this.actualRouteNumber === this.allRouteNumber - 1) {
      this.sPoint = new Point(
        this.accommodationCityList[this.actualRouteNumber - 1]['1'],
        this.accommodationCityList[this.actualRouteNumber - 1]['2'],
        this.accommodationCityList[this.actualRouteNumber - 1]['3']);
      this.ePoint = new Point(
        this.returnedData['endPoint'].lon,
        this.returnedData['endPoint'].lat,
        this.returnedData['endPoint'].name);
      this.isLast = true;
    }
    // else {
    //   this.sPoint = new Point(
    //     this.accommodationCityList[0]['1'],
    //     this.accommodationCityList[0]['2'],
    //     this.accommodationCityList[0]['3']);
    //   this.ePoint = new Point(
    //     this.returnedData['startPoint'].lon,
    //     this.returnedData['startPoint'].lat,
    //     this.returnedData['startPoint'].name);
    //
    // }
    this.setMarkers();
    this.getAllWays();
    this.getCenterBetween();

  }

  prevRoad() {
    if (this.actualRouteNumber !== 0) {
      this.actualRouteNumber--;
    }
    if (this.actualRouteNumber === 0) {
      this.sPoint = new Point(
        this.returnedData['startPoint'].lon,
        this.returnedData['startPoint'].lat,
        this.returnedData['startPoint'].name);
      this.ePoint = new Point(
        this.accommodationCityList['0']['1'],
        this.accommodationCityList['0']['2'],
        this.accommodationCityList['0']['3']);
    } else {
      this.sPoint = new Point(
        this.accommodationCityList[this.actualRouteNumber - 1]['1'],
        this.accommodationCityList[this.actualRouteNumber - 1]['2'],
        this.accommodationCityList[this.actualRouteNumber - 1]['3']);
      this.ePoint = new Point(
        this.accommodationCityList[this.actualRouteNumber]['1'],
        this.accommodationCityList[this.actualRouteNumber]['2'],
        this.accommodationCityList[this.actualRouteNumber]['3']);
    }
    this.setMarkers();
    this.getAllWays();
    this.getCenterBetween();

  }

  saveRouteToList() {
    this.routeList.push([this.actualRouteNumber, this.actualGeometryName]);
    alert('Pomyslnei dodano trasę');
  }

  showSaveDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.position = {
      left: '35%',
      right: '35%',
      top: '10%'
    };
    dialogConfig.width = '25%';
    let dataName;
    let date;
    const dialogRef = this.dialog.open(NamedialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data === null) {
          return;
        }
        dataName = data.name;
        date = data.date;
        const tripData = {
          name: dataName,
          start_name: this.returnedData['startPoint'].name,
          start_lon: this.StartPoinTrip['lon'],
          start_lat: this.StartPoinTrip['lat'],
          end_name: this.returnedData['endPoint'].name,
          end_lon: this.EndPoinTrip['lon'],
          end_lat: this.EndPoinTrip['lat'],
          usage_date: date
        };
        this.loading = true;
        this.saveTripToBase(tripData);

      }
    );
  }

  saveTripToUserAcount() {
    if (this.authService.getIsAuthorize()) {
      if (this.routeList.length === this.allRouteNumber) {
        this.showSaveDialog();

      } else {
        alert('Trasa niepełna');
      }
    } else {
      alert('Zaloguj sie by zapisać');
    }
  }

  allRoadsPush() {
    for (let i = 0; i < this.routeList.length; i++) {
      const route = {
        trip_id: this.createdTripId,
        road_type: this.routeList[i][1],
        road_number_in_trip: this.routeList[i][0]
      };
      this.saveRoad(route);
    }
  }

  allHotelsPush() {
    for (let i = 0; i < this.savedHotelList.length; i++) {
      const hotel = {
        hotel_id: this.savedHotelList[i][1],
        trip_id: this.createdTripId,
        city: this.savedHotelList[i][0],
        hotel_number_trip: i
      };
      if (i === this.savedHotelList.length - 1) {
        this.saveHotel(hotel, true);
      } else {
        this.saveHotel(hotel, false);
      }
    }
  }

  saveRoad(road) {

    const headers = new HttpHeaders()
      .set('Authorization', 'Token ' + this.authService.getToken());


    this.http
      .post(this.apiservice.tripRouteAddUrl, road, {headers: headers})
      .subscribe(
        res => {
          console.log(res);
          this.loading = false;
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  saveHotel(hotel, isLastHotel) {
    const headers = new HttpHeaders()
      .set('Authorization', 'Token ' + this.authService.getToken());


    this.http
      .post(this.apiservice.tripHotelAddUrl, hotel, {headers: headers})
      .subscribe(
        res => {
          if (isLastHotel) {
            this.loading = false;
            alert('Pomyślnei zapisano wycieczkę');
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  saveTripToBase(trip) {
    const headers = new HttpHeaders()
      .set('Authorization', 'Token ' + this.authService.getToken());


    this.http
      .post(this.apiservice.tripcreateUrl, trip, {headers: headers})
      .subscribe(
        res => {
          console.log(res);
          this.createdTripId = res['trip'].id;
          this.allRoadsPush();
          this.allHotelsPush();
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  getCommentsRoad(id) {
    this.loading = true;

    this.http
      .get(this.apiservice.commentRoadList)
      .subscribe(
        res => {
          // this.comments
          const com = res['roads_comments'];
          // if (this.comments.length === 0) {
          //   this.comments = [];
          // }
          this.comments = [];
          for (const comment of com) {
            console.log(id);
            console.log(comment.road_id);
            if (comment.road_id === id) {
              this.comments.push(comment);
            }
          }

          console.log(res);
          this.loading = false;
        },
        error => {
          this.comments = [];
          this.loading = false;
        }
      );
  }

  getCommentsHotel(hotelId) {
    this.loading = true;

    this.http
      .get(this.apiservice.commentHotelList + hotelId)
      .subscribe(
        res => {
          this.comments = res['hotels_comments'];
          if (this.comments.length === 0) {
            this.comments = [];
          }
          console.log(this.comments);
          this.loading = false;
        },
        error => {
          this.comments = [];
          this.loading = false;
        }
      );
  }
}

class Sugestion {
  constructor(
    text: string,
    lon: number,
    lat: number,
    city: string,
    country_code: string,
    country: string
  ) {

  }
}


class Point {
  constructor(
    public lon: number,
    public lat: number,
    public name: string,
  ) {
  }
}

class TripToSave {
  constructor(
    name: string,
    sPoint: Point,
    ePoint: Point,
    hotelDataList: any,
    routesList: any,
  ) {
  }

}

/*
const tripData = {
      startPoint: this.sPoint,
      endPoint: this.ePoint,
      cityTab: this.hotelCityList
    };




cityTab: Array(4)
  0: Array(6)
    0: "Łapy, gmina Łapy, powiat białostocki, województwo podlaskie, 18-100, Polska"
    1: "22.883005"
    2: "52.9908208"
    3: "Łapy"
    4: "pl"
    5: "Polska"
    length: 6
    __proto__: Array(0)
  1: (6) ["Ełk, powiat ełcki, województwo warmińsko-mazurskie, Polska", "22.3615813", "53.8248304", "Ełk", "pl", "Polska"]
  2: (6) ["Poznań, województwo wielkopolskie, Polska", "16.9335199", "52.4082663", "Poznań", "pl", "Polska"]
  3: (6) ["Wrocław, województwo dolnośląskie, Polska", "17.0326689", "51.1089776", "Wrocław", "pl", "Polska"]
  length: 4
__proto__: Array(0)

*/
