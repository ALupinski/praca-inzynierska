import {Component, HostBinding, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {RegisterComponent} from '../register/register.component';
import {HttpClient} from '@angular/common/http';
import {ApiService} from '../apiconect/api.service';
import {Router} from '@angular/router';
import {AuthorizationService} from '../apiconect/authorization.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass'],
})

export class LoginComponent implements OnInit {
  isInvalid: boolean;
  form = new LoginForm('', '');
  passPatern: '^(?=.*[A-Za-z])(?=.*[0-9]).{8,}$';
  private router: Router;

  constructor(private dialog: MatDialog,
              private authService: AuthorizationService,
              private apiservice: ApiService) {

  }

  ngOnInit() {
  }

  // @HostBinding('class.login-container') true;
  passwordReset(event) {
    this.dialog.closeAll();
  }

  loginDialog(event) {
    this.dialog.closeAll();
    this.registerButtonEvent(event);
  }

  registerButtonEvent(event) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.position = {
      left: '35%',
      right: '35%',
      top: '10%'
    };
    dialogConfig.width = '30%';
    this.dialog.open(RegisterComponent, dialogConfig);
  }

  onSubmit() {
    this.login();
  }

  login() {
    this.apiservice.login(this.form).subscribe(
      res => {
        this.isInvalid = false;
        this.authService.setLoggedData(res['token'], res['user_login'], res['id']);
        // localStorage.setItem('email', res['email']);
        this.dialog.closeAll();
        location.reload();
      },
      err => {
        this.isInvalid = true;
      }
    );
  }

}

class LoginForm {
  constructor(
    public user_login: string,
    public password: string,
  ) {
  }
}
