import {Component, OnInit} from '@angular/core';
import {ApiService} from '../apiconect/api.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {turf} from '@turf/turf/turf';
import 'rxjs-compat/add/operator/debounceTime';
import {FormControl} from '@angular/forms';
import {debounceTime, switchMap} from 'rxjs/operators';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {NamedialogComponent} from '../namedialog/namedialog.component';
import {AuthorizationService} from '../apiconect/authorization.service';

declare var ol: any;

@Component({
  selector: 'app-check-road',
  templateUrl: './check-road.component.html',
  styleUrls: ['./check-road.component.sass']
})

export class CheckRoadComponent implements OnInit {
  public message: string;
  form: CheckPathForm;
  public sPoint: Point;
  public ePoint: Point;
  map: any;
  private query: string;
  private iconFeature: any;
  private routeLayer = new ol.layer.Vector();
  private markerSource = new ol.source.Vector();
  public loading = false;
  public fastTime: any;
  public fastDistance: any;
  public shortTime: any;
  public shortDistance: any;
  public recomendTime: any;
  public recomendDistance: any;
  private markerStyle = new ol.style.Style({
    image: new ol.style.Circle({
      radius: 7,
      stroke: new ol.style.Stroke({
        color: '#333333'
      }),
      fill: new ol.style.Fill({
        color: '#333333' // attribute colour
      })
    }),
  });
  private requestedRuteFastest: any;
  private requestedRuteShortest: any;
  private requestedRuteRecomended: any;
  public showRouteResoult = false;
  public showStepsR = false;
  public showStepsF = false;
  public showStepsS = false;
  private actualStepNumber: number;
  private steps: any;
  private stepsArrayLength: number;
  actualStep: any;
  private actualGeometry: any;
  public isToken = false;

  startForm: FormControl;
  endForm: FormControl;

  public errorMessage: string;
  public isError = false;
  public isErrorSec = false;

  cities: Sugestion [] = [
    ['Brak sugesti']
  ];
  citiesSec: Sugestion [] = [
    ['Brak sugesti']
  ];
  private actualGeometryName: string;
  private dataName: any;
  private date: any;
  public showComments = false;
  public comments: any;
  public roadId: string;

  constructor(private apiservice: ApiService,
              private http: HttpClient,
              private dialog: MatDialog,
              private authService: AuthorizationService) {
    this.startForm = new FormControl();
    this.startForm
      .valueChanges
      .pipe(
        switchMap(value => this.cities)
      );
    this.endForm = new FormControl();
    this.endForm
      .valueChanges
      .pipe(
        switchMap(value => this.citiesSec)
      );
  }

  ngOnInit() {
    if (localStorage.getItem('token') !== null) {
      this.isToken = true;
    }
    this.form = new CheckPathForm(null, null);
    this.initMap();
  }

  initMap() {
    this.map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        }),
        new ol.layer.Vector({
          source: this.markerSource,
          style: this.markerStyle,
        }),
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([20, 50]),
        zoom: 4.6,
        projection: 'EPSG:3857',
      }),
    });
    setTimeout(() => this.map.updateSize(), 100);
  }

  submit() {
    if (this.form.start.length !== 0 && this.form.endpoint.length !== 0) {
      this.showComments = false;
      this.showRouteResoult = false;
      this.getAllWays();
    }
  }

  getTimeAndDistance() {
    this.fastDistance = parseFloat(this.requestedRuteFastest['0'].summary['distance']) / 1000;
    this.fastTime = parseFloat(this.requestedRuteFastest['0'].summary['duration']) / 60;
    this.shortDistance = parseFloat(this.requestedRuteShortest['0'].summary['distance']) / 1000;
    this.shortTime = parseFloat(this.requestedRuteShortest['0'].summary['duration']) / 60;
    this.recomendDistance = parseFloat(this.requestedRuteRecomended['0'].summary['distance']) / 1000;
    this.recomendTime = parseFloat(this.requestedRuteRecomended['0'].summary['duration']) / 60;
    this.fastDistance = Math.round(this.fastDistance);
    this.fastTime = Math.round(this.fastTime);
    this.shortDistance = Math.round(this.shortDistance);
    this.shortTime = Math.round(this.shortTime);
    this.recomendDistance = Math.round(this.recomendDistance);
    this.recomendTime = Math.round(this.recomendTime);

  }

  getAllWays() {
    this.getWayF();
    this.getWayS();
    this.getWayR();

  }

  selectSuggest(item, number) {
    this.isError = false;
    this.isErrorSec = false;
    if (item === undefined || item === null) {
      this.errorMessage = 'Nie dodano miasta, spróbuj ponownie';
      if (number === 0) {
        this.isError = true;

      } else {
        this.isErrorSec = true;

      }
    } else {
      if (number === 0) {
        this.sPoint = new Point(item['1'], item['2'], item['0']);
        this.form.start = item['0'];
        this.setMarker(0);

      } else {
        this.ePoint = new Point(item['1'], item['2'], item['0']);
        this.form.endpoint = item['0'];
        this.setMarker(1);
      }
    }

  }

  typed(event, number) {

    this.query = event + '&format=json&addressdetails=1';

    this.http.get(this.apiservice.API_NOMINATIM + this.query).subscribe(
      res => {
        this.cities = [];
        this.citiesSec = [];
        for (let i = 0; i < 10; i++) {
          if (number === 0) {
            try {
              this.cities.push(
                [res[i].display_name,
                  res[i].lon,
                  res[i].lat,
                  res[i].address.city,
                  res[i].address.country_code,
                  res[i].address.country]);
            } catch (e) {
            }
          } else {
            try {
              this.citiesSec.push(
                [res[i].display_name,
                  res[i].lon,
                  res[i].lat,
                  res[i].address.city,
                  res[i].address.country_code,
                  res[i].address.country]);
            } catch (e) {
            }
          }


        }
      },
      error => {
        console.log(error);
      }
    );

  }

  addMapPoint(lon, lat, id) {
    this.iconFeature = new ol.Feature({
      geometry: new ol.geom.Point(ol.proj.transform([parseFloat(lon), parseFloat(lat)], 'EPSG:4326',
        'EPSG:3857')),
      name: 'Null Island',
      population: 4000,
      rainfall: 500,
      id: id
    });

    this.markerSource.addFeature(this.iconFeature);

  }

  setCenter(lon, lat, zoom) {
    const view = this.map.getView();
    view.setCenter(ol.proj.fromLonLat([parseFloat(String(lon)), parseFloat(String(lat))]));
    view.setZoom(zoom);
  }

  setMarker(number) {
    this.clearLayer();
    if (number === 0) {
      if (this.sPoint.name.length !== 0) {
        this.addMapPoint(this.sPoint.lon, this.sPoint.lat, 0);
        this.setCenter(this.sPoint.lon, this.sPoint.lat, 8);
      }
    } else {
      if (this.ePoint.name.length !== 0) {
        this.addMapPoint(this.ePoint.lon, this.ePoint.lat, 1);
        this.setCenter(this.ePoint.lon, this.ePoint.lat, 8);
      }
    }
    if (this.ePoint !== undefined && this.sPoint !== undefined) {
      this.addMapPoint(this.sPoint.lon, this.sPoint.lat, 0);
      this.addMapPoint(this.ePoint.lon, this.ePoint.lat, 1);
      this.lineDraw();
    } else {
      this.message = 'Podaj punkt startowy i docelowy';
    }
  }

  lineDraw() {
    const line_point = [];
    line_point.push(ol.proj.fromLonLat([parseFloat(String(this.sPoint.lon)), parseFloat(String(this.sPoint.lat))]));
    line_point.push(ol.proj.fromLonLat([parseFloat(String(this.ePoint.lon)), parseFloat(String(this.ePoint.lat))]));
    this.routeLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: [new ol.Feature({
          geometry: new ol.geom.LineString(line_point, 'XY'),
          name: 'Line'
        })]
      }),
      style: new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: '#333333',
          width: 4
        })
      })
    });
    this.routeLayer.setZIndex(5);
    this.map.addLayer(this.routeLayer);
  }

  clearLayer() {
    if (this.routeLayer.getSource() !== null) {
      const features = this.routeLayer.getSource().getFeatures();
      features.forEach((feature) => {
        this.routeLayer.getSource().removeFeature(feature);
      });
      this.markerSource.clear(true);
    }
  }

  getWayF() {
    this.loading = true;
    this.http.get(this.apiservice.getOpenRouteUrl(
      this.sPoint.lon,
      this.sPoint.lat,
      this.ePoint.lon,
      this.ePoint.lat,
      'fastest',
      'driving-car'))
      .debounceTime(400)
      .subscribe(
        res => {
          this.requestedRuteFastest = res['routes'];
        },
        error => {
          this.responseErrorCatch(error);
        }
      );
  }

  getWayR() {
    this.http.get(this.apiservice.getOpenRouteUrl(
      this.sPoint.lon,
      this.sPoint.lat,
      this.ePoint.lon,
      this.ePoint.lat,
      'recommended',
      'driving-hgv'))
      .debounceTime(400)
      .subscribe(
        res => {
          this.requestedRuteRecomended = res['routes'];
          this.loading = false;
          this.getTimeAndDistance();
          this.showRouteResoult = true;
        },
        error => {
          this.responseErrorCatch(error);
        }
      );
  }

  getWayS() {
    this.http.get(this.apiservice.getOpenRouteUrl(
      this.sPoint.lon,
      this.sPoint.lat,
      this.ePoint.lon,
      this.ePoint.lat,
      'shortest',
      'driving-car'))
      .debounceTime(400)
      .subscribe(
        res => {
          this.requestedRuteShortest = res['routes'];
        },
        error => {
          this.responseErrorCatch(error);
          console.log(error);
        }
      );
  }

  setActualData(json, name) {
    this.actualGeometry = json['0'].geometry;
    this.actualGeometryName = name;
    this.steps = json['0'].segments['0'].steps;
    this.stepsArrayLength = json['0'].segments['0'].steps.length;
    this.actualStepNumber = 0;
    this.actualStep = this.steps[this.actualStepNumber];
    this.actualStep.instruction = this.returnInstruction(this.actualStep.type, this.actualStep.name, this.actualStep.distance);
  }

  setShowFalse() {
    this.showStepsR = false;
    this.showStepsS = false;
    this.showStepsF = false;

  }

  chooseRoute(number) {
    this.showComments = true;
    this.setShowFalse();
    switch (number) {
      case 1: {
        this.setActualData(this.requestedRuteFastest, 'F');
        this.showStepsF = true;
        this.importRoute(this.actualGeometry);

        // this.setPositionMapPoint();
        break;
      }
      case 2: {
        this.setActualData(this.requestedRuteShortest, 'S');
        this.showStepsS = true;
        this.importRoute(this.actualGeometry);
        // this.setPositionMapPoint();
        break;
      }
      case 3: {
        this.setActualData(this.requestedRuteRecomended, 'R');
        this.showStepsR = true;
        this.importRoute(this.actualGeometry);
        // this.setPositionMapPoint();
        break;
      }
      default: {
        this.setActualData(this.requestedRuteRecomended, 'R');
        this.showStepsR = true;
        this.importRoute(this.actualGeometry);
        // this.setPositionMapPoint();
        break;
      }
    }
    this.roadId = this.sPoint.lon.toString() +
      this.sPoint.lat.toString() +
      this.ePoint.lon.toString() +
      this.ePoint.lat.toString() +
      this.actualGeometryName.toString();
    this.roadId = this.roadId.replace(/\./g, '');
    this.getComments(this.roadId);
  }

  importRoute(geometry) {
    const features = this.routeLayer.getSource().getFeatures();
    features.forEach((feature) => {
      this.routeLayer.getSource().removeFeature(feature);
    });
    const pointarray = [];
    for (let i = 0; i < geometry.length; i++) {
      pointarray.push([geometry[i][0], geometry[i][1]]);
    }
    const polyline = new ol.geom.LineString(pointarray);
    polyline.transform('EPSG:4326', 'EPSG:3857');

    this.routeLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: [new ol.Feature({
          geometry: polyline,
          name: 'Line'
        })]
      }),
      style: new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: '#333333',
          width: 3
        })
      })
    });
    this.routeLayer.setZIndex(5);
    this.map.addLayer(this.routeLayer);

  }

  nextStep() {
    if (this.actualStepNumber !== this.stepsArrayLength) {
      this.actualStepNumber++;
      this.actualStep = this.steps[this.actualStepNumber];
      this.actualStep.instruction = this.returnInstruction(this.actualStep.type, this.actualStep.name, this.actualStep.distance);
      this.setPositionMapPoint();
    }
  }

  previousStep() {
    if (this.actualStepNumber !== 0) {
      this.actualStepNumber--;
      this.actualStep = this.steps[this.actualStepNumber];
      this.actualStep.instruction = this.returnInstruction(this.actualStep.type, this.actualStep.name, this.actualStep.distance);
      this.setPositionMapPoint();
    }
  }

  setPositionMapPoint() {
    const num = this.actualStep['way_points'][0];
    this.setCenter(this.actualGeometry[num][0], this.actualGeometry[num][1], 17);
  }

  responseErrorCatch(error) {
    let errorMessage: any;
    this.loading = false;
    switch (error['error']['error']['message']) {
      case 'Request parameters exceed the server configuration limits. ' +
      'By dynamic weighting, the approximated distance of a route segment must not be greater than 300000.0 meters.': {
        errorMessage = 'Wybrane punkty są za daleko od siebie';
        break;
      }
      default: {
        errorMessage = error['error']['error']['message'];
        break;
      }
    }
    alert(errorMessage);
  }

  returnInstruction(type, name, dist) {
    let instruction: string;
    switch (type) {
      case 0: {
        instruction = 'Za ' + dist + ' metrów skręć w lewo ';
        break;
      }
      case 1: {
        instruction = 'Za ' + dist + ' metrów skręć w prawo ';

        break;
      }
      case 2: {
        instruction = 'Za ' + dist + ' metrów skręć ostro w lewo ';
        break;
      }
      case 3: {
        instruction = 'Za ' + dist + ' metrów skręć ostro w prawo ';
        break;
      }
      case 4: {
        instruction = 'Za ' + dist + ' metrów skręć lekko w lewo ';
        break;
      }
      case 5: {
        instruction = 'Za ' + dist + ' metrów skręć lekko w prawo ';
        break;
      }
      case 6: {
        instruction = 'Jedź dalej prosto ';
        break;
      }
      case 7: {
        instruction = 'Na rondzie drugi zjazd ';
        break;
      }
      case 8: {
        instruction = 'Na rondzie trzeci zjazd ';
        break;
      }
      case 9: {
        instruction = 'Na rondzie czwarty zjazd ';
        break;
      }
      case 10: {
        instruction = 'Dotarłeś do celu ';
        break;
      }
      case 11: {
        instruction = 'Kieruj się na północ przez ' + dist + ' metrów';
        break;
      }
      case 12: {
        instruction = 'Trzymaj się lewej strony ';
        break;
      }
      case 13: {
        instruction = 'Trzymaj się prawej strony ';
        break;
      }
    }
    if (name !== '') {
      instruction = instruction + 'na ' + name;
    }
    return instruction;
  }

  showSaveDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = false;
    dialogConfig.position = {
      left: '35%',
      right: '35%',
      top: '10%'
    };
    dialogConfig.width = '25%';

    const dialogRef = this.dialog.open(NamedialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data === null) {
          return;
        }
        this.roadId = this.sPoint.lon.toString() +
          this.sPoint.lat.toString() +
          this.ePoint.lon.toString() +
          this.ePoint.lat.toString() +
          this.actualGeometryName.toString();
        this.roadId = this.roadId.replace(/\./g, '');
        this.dataName = data.name;
        this.date = data.date;
        console.log(data.date);
        const road = {
          name: this.dataName,
          sPointName: this.sPoint.name,
          ePointName: this.ePoint.name,
          slon: this.sPoint.lon,
          slat: this.sPoint.lat,
          elon: this.ePoint.lon,
          elat: this.ePoint.lat,
          geometryName: this.actualGeometryName,
          usage_date: this.date,
          road_id: this.roadId,
        };
        this.saveRoadToBase(road);
      }
    );

  }


  saveRouteToAcount() {
    if (!this.authService.getIsAuthorize()) {
      alert('Zaloguj się żeby zapisać trasę ');
      return;
    }
    this.showSaveDialog();

  }

  saveRoadToBase(road) {
    this.loading = true;

    const headers = new HttpHeaders()
      .set('Authorization', 'Token ' + this.authService.getToken());


    this.http
      .post(this.apiservice.roadcreateUrl, road, {headers: headers})
      .subscribe(
        res => {
          this.loading = false;
          alert('Pomyślnie dodano trasę ');
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  getComments(id) {
    this.loading = true;

    this.http
      .get(this.apiservice.commentRoadList)
      .subscribe(
        res => {
          // this.comments
          const com = res['roads_comments'];
          // if (this.comments.length === 0) {
          //   this.comments = [];
          // }
          this.comments = [];
          for (const comment of com) {
            console.log(id);
            console.log(comment.road_id);
            if (comment.road_id === id) {
              this.comments.push(comment);
            }
          }

          console.log(res);
          this.loading = false;
        },
        error => {
          this.comments = [];
          this.loading = false;
        }
      );
  }
}

class CheckPathForm {
  constructor(
    public start: string,
    public endpoint: string,
  ) {
  }
}

class Point {
  constructor(
    public lon: number,
    public lat: number,
    public name: string,
  ) {
  }
}

class Sugestion {
  constructor(
    text: string,
    lon: number,
    lat: number,
    city: string,
    country_code: string,
    country: string
  ) {

  }
}

