import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckRoadComponent } from './check-road.component';

describe('CheckRoadComponent', () => {
  let component: CheckRoadComponent;
  let fixture: ComponentFixture<CheckRoadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckRoadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckRoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
