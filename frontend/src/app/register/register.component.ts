import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {LoginComponent} from '../login/login.component';
import {ApiService} from '../apiconect/api.service';
import {Router} from '@angular/router';
import {AuthorizationService} from '../apiconect/authorization.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {
  isInvalid: boolean ;
  form = new RegisterForm('', '', '');
  passPatern: '^(?=.*[A-Za-z])(?=.*[0-9]).{8,}$';
  private router: Router;
  constructor(private dialog: MatDialog,
              private apiservice: ApiService,
              private authService: AuthorizationService,) { }

  ngOnInit() {
    this.isInvalid = false;
  }
  GoToLogin(event) {
    this.dialog.closeAll();
    this.logginButtonEvent(event);
  }
  logginButtonEvent(event) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.position = {
      left: '35%',
      right: '35%',
      top: '10%'
    };
    dialogConfig.width = '30%';
    this.dialog.open(LoginComponent, dialogConfig);
  }
  onSubmit() {
    this.register();
  }
  register() {
    console.log(this.form);
    this.apiservice.register(this.form).subscribe(
      res => {
        localStorage.setItem('token', res['token']);
        localStorage.setItem('user_login', res['user_login']);
        this.authService.setLoggedData(res['token'], res['user_login'], res['id']);

        this.dialog.closeAll();
        location.reload();
      },
      err => {
        this.isInvalid = true;
      }
    );
  }
}
class RegisterForm {
  constructor(
    public user_login: string,
    public email: string,
    public password: string,
  ) {}
}
