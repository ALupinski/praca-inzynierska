import {Component, OnInit, ViewContainerRef} from '@angular/core';
import { Http } from '@angular/http';
import {LoginComponent} from './login/login.component';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {RegisterComponent} from './register/register.component';
import {AuthorizationService} from './apiconect/authorization.service';
import {Router} from "@angular/router";
// import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  isAuth = false;
  user_login: string;
  logginButtonEvent(event) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.position = {
        left: '35%',
        right: '35%',
        top: '10%'
      };
    dialogConfig.width = '30%';
    this.dialog.open(LoginComponent, dialogConfig);
  }
  registerButtonEvent(event) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.position = {
      left: '35%',
      right: '35%',
      top: '10%'
    };
    dialogConfig.width = '30%';
    this.dialog.open(RegisterComponent, dialogConfig);
  }
  constructor(private http: Http,
              private dialog: MatDialog,
              private router: Router,
              private  auth_service: AuthorizationService) {
  // , public toastr: ToastsManager, vcr: ViewContainerRef
  //   this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
    if (this.auth_service.getToken() !== null) {
      this.isAuth = true;
      this.user_login = this.auth_service.getUserLogin();
    }
  }

  logoutButtonEvent() {
    this.auth_service.removeLocalStorageData();
    location.reload();
  }

  homeRedirectButtonEvent() {
    this.router.navigate(['/home']);
  }

  userButtonEvent() {
    if (this.auth_service.getToken() !== null) {
      this.router.navigate(['/user']);
    } else {
      // this.toastr.error('This is not good!', 'Oops!');
    }
  }
}

