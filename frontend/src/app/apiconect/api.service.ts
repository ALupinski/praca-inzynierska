import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private API_DEVELOPMENT = 'http://127.0.0.1:8000/api/';
  API_NOMINATIM = 'https://nominatim.openstreetmap.org/search?city=';

  private loginUrl = this.API_DEVELOPMENT + 'user-login/';
  private registerUrl = this.API_DEVELOPMENT + 'create-account/';

  public roadcreateUrl = this.API_DEVELOPMENT + 'user-road-create/';
  public roadUserListUrl = this.API_DEVELOPMENT + 'user-roads/';
  public roadDeleteUrl = this.API_DEVELOPMENT + 'user-road-destroy/';

  public hotelcreateUrl = this.API_DEVELOPMENT + 'user-hotel-create/';
  public hotelUserListUrl = this.API_DEVELOPMENT + 'user-hotels/';
  public hotelDeleteUrl = this.API_DEVELOPMENT + 'user-hotel-destroy/';

  public tripcreateUrl = this.API_DEVELOPMENT + 'trip-create/';
  public tripUserListUrl = this.API_DEVELOPMENT + 'user-trips-list/';
  public tripDeleteUrl = this.API_DEVELOPMENT + 'user-trip-destroy/';
  public tripHotelAddUrl = this.API_DEVELOPMENT + 'trip-hotel-add/';
  public tripRouteAddUrl = this.API_DEVELOPMENT + 'trip-road-add/';

  public commentHotelList = this.API_DEVELOPMENT + 'hotel-comments-list/';
  public commentHotelCreate = this.API_DEVELOPMENT + 'hotel-comment-create/';

  public commentRoadList = this.API_DEVELOPMENT + 'road-comments-list/';
  public commentRoadCreate = this.API_DEVELOPMENT + 'road-comment-create/';

  private openrouteKey = '5b3ce3597851110001cf6248c5b550560b934f91b14edac4829d7a47';
  private sabreTokenKey = 'T1RLAQKOiVJCjxa747GBn5B4K0vYczUuDBBnkSkP+tDnk3A6GBPqqG+nAADAw8ZuIEydK4VYJlje/j9YYhq8mU++8p6ox3X3b/y1X0wXJmvf2f+r/GzKr/ve9YFokUEWf6e4hG3aZbo8OYuVvWwpAMP5ReZG04yQ3mSI2wL9aDdyBtVhGmIpArloLqZTdNemmvQ+tRNvv5c5gSxfoLlLXy20Ka77dO6+HkftThUzy3wnd2N6HNpqDJa9V+s54CjkGDCqCobjvIXrpnjT6LisFnzb1v5DE0zqMVKr5EJFtVsb8XepAQR8y8uC36iv';
  private sabreHotelDescription = 'https://api-crt.cert.havail.sabre.com/v1.0.0/shop/hotels/description?mode=description';
  private sabreUrlHotelImage = 'https://api-crt.cert.havail.sabre.com/v1.0.0/shop/hotels/image?mode=image';
  private geoSearchSabre = 'https://api-crt.cert.havail.sabre.com/v1.0.0/lists/utilities/geosearch/locations?mode=geosearch';
  private openrouteUrl = 'https://api.openrouteservice.org/directions?&api_key=';

  public constructor(private http: HttpClient) {
  }

  login(user) {
    return this.http.post(this.loginUrl, user);
  }

  register(user) {
    return this.http.post(this.registerUrl, user);
  }

  getOpenRouteUrl(StartLon, StartLat, EndLon, EndLat, preference, profile) {
    const url = this.openrouteUrl +
      this.openrouteKey +
      '&coordinates=' + StartLon + ',' + StartLat + '|' + EndLon + ',' + EndLat +
      '&profile=' + profile +
      '&preference=' + preference +
      '&format=json' +
      '&geometry_format=polyline';
    return url;
  }

  getSabreUrl() {
    return this.geoSearchSabre;
  }

  getSabreToken() {
    return this.sabreTokenKey;
  }

  getSabreImageUrl() {
    return this.sabreUrlHotelImage;
  }

  getSabreDescriptionUrl() {
    return this.sabreHotelDescription;
  }


}





