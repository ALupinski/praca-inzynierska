import { TestBed, inject } from '@angular/core/testing';

import { AuthorizationService } from './authorization.service';
import {Injectable} from '@angular/core';

describe('AuthorizationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthorizationService]
    });
  });

  it('should be created', inject([AuthorizationService], (service: AuthorizationService) => {
    expect(service).toBeTruthy();
  }));
});
