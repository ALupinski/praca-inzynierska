import { TestBed, inject } from '@angular/core/testing';

import { MyselectService } from './myselect.service';

describe('MyselectService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyselectService]
    });
  });

  it('should be created', inject([MyselectService], (service: MyselectService) => {
    expect(service).toBeTruthy();
  }));
});
