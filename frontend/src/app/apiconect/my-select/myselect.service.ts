import {Injectable} from '@angular/core';
import {ApiService} from '../api.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MyselectService {
  public suggestionlist: any;
  private showSuggestion: boolean;
  private query: string;

  typed(event, number) {

    this.query = event + '&format=json&addressdetails=1';

    this.http.get(this.apiservice.API_NOMINATIM + this.query).subscribe(
      res => {
        this.suggestionlist = res;
          this.showSuggestion = true;
      },
      error => {
        console.log(error);
      }
    );

  }
  selectSuggest(item) {
      this.showSuggestion = false;
      return new Point(item['lon'], item['lat'], item['display_name']);


  }

  constructor(private apiservice: ApiService,
              private http: HttpClient) {
  }
}
class Point {
  constructor(
    public lon: number,
    public lat: number,
    public name: string,
  ) {
  }
}

