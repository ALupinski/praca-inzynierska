import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  private isAutorize = false;

  constructor() {
  }

  public getUserLogin() {
    return localStorage.getItem('user_login');
  }

  public getToken() {
    return localStorage.getItem('token');
  }

  public getEmail() {
    return localStorage.getItem('email');
  }

  public setLoggedData(token, user_login, id) {
    localStorage.setItem('token', token);
    localStorage.setItem('user_login', user_login);
    localStorage.setItem('isAutorize', 'true');
  }

  public removeLocalStorageData() {
    localStorage.removeItem('token');
    localStorage.removeItem('user_login');
    localStorage.setItem('isAutorize', 'false');

    // localStorage.removeItem('email');
  }

  getIsAuthorize() {
    if (localStorage.getItem('isAutorize') === 'true') {
      return true;
    } else {
      return false;
    }

  }

  setIsAuthorize(authorize) {
    this.isAutorize = authorize;
  }

}
