import json

from rest_framework.renderers import JSONRenderer


class CustomJSONRenderer(JSONRenderer):
    charset = 'utf-8'
    object_label = 'object'
    message = 'message'
    status = 'success'

    def render(self, data, media_type=None, renderer_context=None):

        if data is not None:

            if 'errors' in data:  # is not None:
                errors = data.get('errors', '')
                self.status = 'error'
                return json.dumps({
                    'status': self.status,
                    'errors': errors,

                })

            return json.dumps({
                'status': self.status,
                'message': str(self.message),
                self.object_label: data,

            })
        return json.dumps({
            'status': self.status,
            'message': str(self.message)
        })


# class PasswordResetJSONRenderer(CustomJSONRenderer):
#     object_label = 'password_reset'
#     message = 'Link resetujący hasło został wysłany na podany adres e-mail.'
#
#
# class PasswordResetConfirmJSONRenderer(CustomJSONRenderer):
#     message = 'Hasło zostało zresetowane.'
#
#
# class PasswordChangeConfirmJSONRenderer(CustomJSONRenderer):
#     message = 'Hasło zostało zmienione.'
