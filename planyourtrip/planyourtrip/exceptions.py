from rest_framework.views import exception_handler
from rest_framework.exceptions import APIException


def core_exception_handler(exc, context):
    response = exception_handler(exc, context)
    handlers = {
        'NotFound': _handle_generic_error,
        'MethodNotAllowed': _handle_generic_error,
        'ValidationError': _handle_generic_error,
        'PermissionDenied': _handle_generic_error,
        'AuthenticationFailed': _handle_generic_error,
        'NotAuthenticated': _handle_generic_error,
        'UserDoesNotExist': _handle_generic_error,
        'HttpErrorResponse': _handle_generic_error,
        'UserHotelExists': _handle_generic_error,
        'UserRoadExists': _handle_generic_error,
        'UserRoadDoesNotExist': _handle_generic_error,
        'UserHotelDoesNotExist':_handle_generic_error,
        'RoadDoesNotExist':_handle_generic_error,
        'HotelDoesNotExist':_handle_generic_error,
        'TripDoesNotExist':_handle_generic_error,
        'CommentDoesExist':_handle_generic_error

    }
    exception_class = exc.__class__.__name__

    if exception_class in handlers:
        return handlers[exception_class](exc, context, response)

    return response


def _handle_generic_error(exc, context, response):
    response.data = {
        'errors': response.data
    }

    return response
