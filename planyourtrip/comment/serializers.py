from rest_framework import serializers
from comment.models import Comment
from hotel.models import CommentHotel
from road.models import CommentRoad


class CommentSerializer(serializers.ModelSerializer):
    id = serializers.CharField(read_only=True)

    class Meta:
        model = Comment
        fields = '__all__'


class HotelCommentSerializer(serializers.ModelSerializer):
    hotel_id = serializers.CharField()
    comment_id = serializers.CharField()

    class Meta:
        model = CommentHotel
        fields = ['hotel_id', 'comment_id']


class HotelsCommentsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ['title', 'content', 'rate', 'created_date', 'user', 'user_login']


class RoadCommentSerializer(serializers.ModelSerializer):
    road_id = serializers.CharField()
    comment_id = serializers.CharField()

    class Meta:
        model = CommentRoad
        fields = ['road_id', 'comment_id']


class RoadsCommentsListSerializer(serializers.ModelSerializer):
    road_id = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = ['title', 'content', 'rate', 'created_date', 'user', 'user_login', 'road_id']

    def get_road_id(self, data):
        commentRoadId = CommentRoad.objects.get(comment_id=data.id).road_id
        return commentRoadId
