from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from User.models import User
from comment.exceptions import CommentDoesExist
from comment.models import Comment
from comment.renderers import CommentCreateJSONRenderer, HotelsCommentsListJSONRenderer, RoadsCommentsListJSONRenderer
from comment.serializers import CommentSerializer, HotelCommentSerializer, HotelsCommentsListSerializer, \
    RoadsCommentsListSerializer, RoadCommentSerializer
from hotel.exceptions import UserDoesNotExist, HotelDoesNotExist
from hotel.models import Hotel, CommentHotel
from road.models import Road, CommentRoad


class CommentCreateAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    # hotel_serializer_class = HotelSerializer
    # user_hotel_serializer_class = HotelUserSerializer
    renderer_classes = (CommentCreateJSONRenderer,)

    def post(self, request, *args, **kwargs):
        data = request.data
        user = request.user
        user_id = request.user.id
        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            raise UserDoesNotExist

        hotel_id = request.data['hotel_id']
        # try:
        #     hotel = Hotel.objects.get(id=hotel_id)
        # except Hotel.DoesNotExist:
        #     raise HotelDoesNotExist

        comment_data = {
            'title': data['title'],
            'content': data['content'],
            'rate': data['rate'],
            'user': user_id,
            'user_login': data['user_login'],
        }

        new_comment_serializer = CommentSerializer(data=comment_data)
        new_comment_serializer.is_valid(raise_exception=True)
        new_comment_serializer.save()

        hotel_comment = {
            'hotel_id': hotel_id,
            'comment_id': new_comment_serializer.data['id']
        }
        hotel_comment_serializer = HotelCommentSerializer(data=hotel_comment)
        hotel_comment_serializer.is_valid(raise_exception=True)
        hotel_comment_serializer.save()

        return Response(new_comment_serializer.data, status=status.HTTP_201_CREATED)


class HotelsCommentsListAPIView(ListAPIView):
    serializer_class = HotelsCommentsListSerializer
    queryset = CommentHotel.objects.all()
    renderer_classes = (HotelsCommentsListJSONRenderer,)

    def list(self, request, id, *args, **kwargs):
        hotels_comments = []
        hotel_id = id
        queryset = self.get_queryset().filter(hotel_id=hotel_id)

        for q in queryset:
            hotels_comments.append(Comment.objects.get(id=q.comment_id))

        serializer = self.serializer_class(hotels_comments, many=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class RoadCommentCreateAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = (CommentCreateJSONRenderer,)

    def post(self, request, *args, **kwargs):
        data = request.data
        user = request.user
        user_id = request.user.id
        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            raise UserDoesNotExist

        road_id = request.data['road_id']

        comment_data = {
            'title': data['title'],
            'content': data['content'],
            'rate': data['rate'],
            'user': user_id,
            'user_login': data['user_login'],
        }

        new_comment_serializer = CommentSerializer(data=comment_data)
        new_comment_serializer.is_valid(raise_exception=True)
        new_comment_serializer.save()

        road_comment = {
            'road_id': road_id,
            'comment_id': new_comment_serializer.data['id']
        }
        road_comment_serializer = RoadCommentSerializer(data=road_comment)
        road_comment_serializer.is_valid(raise_exception=True)
        road_comment_serializer.save()

        return Response(new_comment_serializer.data, status=status.HTTP_201_CREATED)


class RoadsCommentsListAPIView(ListAPIView):
    serializer_class = RoadsCommentsListSerializer
    queryset = CommentRoad.objects.all()
    renderer_classes = (RoadsCommentsListJSONRenderer,)

    def list(self, request, *args, **kwargs):
        roads_comments = []
        queryset = self.get_queryset()
        
        for q in queryset:
            roads_comments.append(Comment.objects.get(id=q.comment_id))

        serializer = self.serializer_class(roads_comments, many=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
