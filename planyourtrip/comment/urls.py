from django.conf.urls import url
from rest_framework import routers

from comment.views import CommentCreateAPIView, HotelsCommentsListAPIView,RoadsCommentsListAPIView,RoadCommentCreateAPIView
from hotel.views import HotelCreateAPIView, UserHotelsListAPIView, UserHotelDestroyAPIView

app_name = 'comment'

urlpatterns = [
    url(r'^hotel-comment-create/', CommentCreateAPIView.as_view(), name='add-hotel-comment'),
    url(r'^hotel-comments-list/(?P<id>\d+)/?$', HotelsCommentsListAPIView.as_view(), name='hotels-comment'),
    url(r'^road-comments-list/', RoadsCommentsListAPIView.as_view(), name='road-comment'),
    url(r'^road-comment-create/', RoadCommentCreateAPIView.as_view(), name='add-road-comment'),


]
