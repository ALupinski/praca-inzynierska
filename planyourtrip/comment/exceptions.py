from rest_framework.exceptions import APIException


class CommentDoesExist(APIException):
    status_code = 400
    default_detail = 'Wpisany komentarz juz istnieje'
