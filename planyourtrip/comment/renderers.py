from planyourtrip.renderers import CustomJSONRenderer


class CommentCreateJSONRenderer(CustomJSONRenderer):
    object_label = 'comment'
    message = 'Nowy komentarz został dodany'


class HotelsCommentsListJSONRenderer(CustomJSONRenderer):
    object_label = 'hotels_comments'
    message = 'Lista komentarzy do hoteli:'

class RoadsCommentsListJSONRenderer(CustomJSONRenderer):
    object_label = 'roads_comments'
    message = 'Lista komentarzy do tras:'

