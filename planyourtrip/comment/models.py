from django.db import models
from User.models import User


# Create your models here.

class Comment(models.Model):
    title = models.CharField(max_length=100, blank=True, null=True)
    content = models.TextField(max_length=999, blank=True, null=True)
    rate = models.CharField(max_length=10)
    created_date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    user_login = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        unique_together = ('user', 'title', 'content', 'rate')




