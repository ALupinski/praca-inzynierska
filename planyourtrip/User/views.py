from builtins import TypeError

from django.shortcuts import render

# Create your views here.
from User.renderers import UserRetrieveJSONRenderer, UserUpdateJSONRenderer
from User.serializers import (RegisterUserSerializer, UserSerializer, LoginSerializer)
from rest_framework.authtoken.models import Token
from rest_framework.generics import RetrieveAPIView, UpdateAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, authentication
from rest_framework.permissions import AllowAny, IsAuthenticated
from .models import User
from .exceptions import (UserNotLoggedIn, UserDataNotValid)


class UserViewSet(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def retrieve(self, request, *args, **kwargs):
        serializer = self.serializer_class(request.user)
        if not request.user.is_authenticated:
            raise UserNotLoggedIn
        return Response(serializer.data, status=status.HTTP_200_OK)


class CreateUserView(APIView):
    """
    Creates the user
    """
    permission_classes = (AllowAny,)

    def post(self, request, format='json'):
        serializer = RegisterUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                token, created = Token.objects.get_or_create(user=user)
                json = serializer.data
                json['token'] = token.key
                return Response(json, status=status.HTTP_201_CREATED)
        else:
            raise TypeError('Invalid data')


class LoginAPIView(APIView):
    # permission_classes = (AllowAny,)
    # renderer_classes = (LoginJSONRenderer,)
    serializer_class = LoginSerializer

    def post(self, request):
        user_data = request.data
        serializer = self.serializer_class(data=user_data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class Logout(APIView):
    queryset = User.objects.all()

    def get(self, request, format=None):
        # simply delete the token to force a login
        request.user.auth_token.delete()

        return Response(status=status.HTTP_200_OK)


class EditUserAPIView(APIView):

    def update(self, request):
        user_data = request.data.get('user', {})
        if not request.user.is_authenticated:
            raise UserNotLoggedIn
        serializer_data = {
            'user_login': user_data.get('user_login', request.user.user_login),
            'email': user_data.get('email', request.user.email),
            'first_name': user_data.get('first_name', request.user.first_name),
            'last_name': user_data.get('last_name', request.user.last_name),
            'home_city': user_data.get('home_city', request.user.home_city),
            'street': user_data.get('street', request.user.street),
            'house_number': user_data.get('house_number', request.user.house_number),
            'zip_code': user_data.get('zip_code', request.user.zip_code),
            'profile': {
                'bio': user_data.get('bio', request.user.profile.bio),
            },
            'image': user_data.get('image', request.user.image)
        }
        serializer = UserSerializer(request.user, data=serializer_data)
        if serializer.is_valid():
            raise UserDataNotValid
        serializer.save()


class UserRetrieveAPIView(RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = (UserRetrieveJSONRenderer,)
    serializer_class = UserSerializer

    def retrieve(self, request, *args, **kwargs):
        serializer = self.serializer_class(request.user)
        # request.override_renderer = UserUpdateJSONRenderer
        return Response(serializer.data, status=status.HTTP_200_OK)


class UserUpdateAPIView(UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = (UserUpdateJSONRenderer,)
    serializer_class = UserSerializer

    def update(self, request, *args, **kwargs):
        user_data = request.data
        serializer_data = {
            'user_login': user_data.get('username', request.user.user_login),
            'email': user_data.get('email', request.user.email),
            'first_name': user_data.get('first_name', request.user.first_name),
            'last_name': user_data.get('last_name', request.user.last_name),
            'home_city': user_data.get('pesel', request.user.home_city),
            'street': user_data.get('pesel', request.user.street),
            'house_number': user_data.get('pesel', request.user.house_number),
            'zip_code': user_data.get('pesel', request.user.zip_code),
        }

        serializer = self.serializer_class(
            request.user, data=serializer_data, partial=True
        )
        serializer.is_valid(raise_exception=True)

        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)
