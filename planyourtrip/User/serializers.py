from rest_framework import serializers, authentication
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token

from Profile.serializers import ProfileSerializer
from rest_framework.validators import UniqueValidator
from .models import User


class RegisterUserSerializer(serializers.ModelSerializer):
    user_login = serializers.CharField(
        max_length=30,
        min_length=5,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True
    )

    def create(self, newUserData):
        user = User.objects.create_user(newUserData['user_login'], newUserData['password'], newUserData['email'])
        return user

    class Meta:
        model = User
        fields = ['user_login', 'email', 'password', 'first_name', 'last_name', 'home_city', 'street', 'house_number',
                  'zip_code']


class LoginSerializer(serializers.ModelSerializer):
    user_login = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)

    class Meta:
        model = User
        # fields = ['user_login', 'password']
        fields = ['id', 'user_login', 'password', 'token']

    def validate(self, data):
        login = data.get('user_login', None)
        password = data.get('password', None)
        if login is None:
            raise serializers.ValidationError('Login is required!')
        if password is None:
            raise serializers.ValidationError('Password is required!')

        user = authenticate(username=login, password=password)
        token, created = Token.objects.get_or_create(user=user)

        if user is None:
            raise serializers.ValidationError(
                'A user with this email and password was not found.'
            )

        return {
            'id': user.id,
            'email': user.email,
            'user_login': user.user_login,
            'token': token
        }


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['user_login', 'email', 'first_name', 'last_name', 'home_city', 'street', 'house_number',
                  'zip_code', 'profile']

    def update(self, instance, validated_data):
        for (key, value) in validated_data.items():
            setattr(instance, key, value)

        instance.save()
        print(instance.first_name)

        return instance
