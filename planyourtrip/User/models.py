from django.db import models
# Create your models here.
from Profile.models import Profile
from django.contrib.auth.models import (AbstractBaseUser, PermissionsMixin, BaseUserManager)
from builtins import TypeError



class ManagerUser(BaseUserManager):
    def create_user(self, user_login, password=None, email=None):
        if user_login is None:
            raise TypeError('Login is required')
        if password is None:
            raise TypeError('Password is required')
        if email is None:
            raise TypeError('Email is required')
        user = self.model(user_login=user_login, email=self.normalize_email(email))
        user.set_password(password)
        user.is_staff = False
        user.is_superuser = False
        user.save()
        profile = Profile.objects.create(user=user)
        profile.save()
        return user
    def create_superuser(self,user_login, password, email = None):
        if user_login is None:
            raise TypeError('Login is required')
        if password is None:
            raise TypeError('Password is required')
        if email is None:
            raise TypeError('Email is required')
        superuser = self.create_user(user_login,password,email)
        superuser.is_staff = True
        superuser.is_superuser = True
        superuser.save()
        return superuser

class User(AbstractBaseUser, PermissionsMixin):
    user_login = models.CharField(max_length=30, blank=True, null=True, unique=True)
    email = models.EmailField(max_length=30, blank=True, null=True, unique=True)
    first_name = models.CharField(max_length=30, blank=True, null=True)
    last_name = models.CharField(max_length=30, blank=True, null=True)
    home_city = models.CharField(max_length=30, blank=True, null=True)
    street = models.CharField(max_length=40, blank=True, null=True)
    house_number = models.CharField(max_length=30, blank=True, null=True)
    zip_code = models.CharField(max_length=9, blank=True, null=True)
    is_staff = models.BooleanField(default=False)

    objects = ManagerUser()
    USERNAME_FIELD = 'user_login'
    # REQUIRED_FIELDS = ['email']
    def __meta__(self):
        return self

