from django.conf.urls import url
from rest_framework import routers
from .views import (UserViewSet, CreateUserView, LoginAPIView, EditUserAPIView, UserRetrieveAPIView, UserUpdateAPIView)



app_name = 'User'

urlpatterns = [
    url(r'^user/', UserViewSet.as_view(), name='user'),
    url(r'^create-account/', CreateUserView.as_view(), name='create-account'),
    url(r'^user-login/', LoginAPIView.as_view(), name='login'),
    url(r'^user-account/', UserRetrieveAPIView.as_view(), name='user-account'),
    url(r'^user-account-update/', UserUpdateAPIView.as_view(), name='edit-account'),

]
