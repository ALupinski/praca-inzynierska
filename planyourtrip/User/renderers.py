from planyourtrip.renderers import CustomJSONRenderer


class UserRetrieveJSONRenderer(CustomJSONRenderer):
    object_label = 'user'
    message = 'Dane użytkownika wyświetlone.'

class UserUpdateJSONRenderer(CustomJSONRenderer):
    object_label = 'user'
    message = 'Informacje profilowe zaktualizowane.'
