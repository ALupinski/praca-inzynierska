from rest_framework.exceptions import APIException


class UserNotLoggedIn(APIException):
    status_code = 401
    default_detail = 'Unauthorized access.'

class UserDataNotValid(APIException):
    status_code = 401
    default_detail = 'Invalid data.'
