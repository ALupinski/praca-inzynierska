from django.apps import AppConfig


class FindtransportConfig(AppConfig):
    name = 'findtransport'
