from django.conf.urls import url
from rest_framework import routers

from hotel.views import HotelCreateAPIView, UserHotelsListAPIView, UserHotelDestroyAPIView
from trip.views import TripCreateAPIView, TripHotelAddAPIView, TripRoadAddAPIView, UserTripsListAPIView, \
    TripDestroyAPIView

app_name = 'trip'

urlpatterns = [
    url(r'^trip-create/', TripCreateAPIView.as_view(), name='trip-create'),
    url(r'^trip-hotel-add/', TripHotelAddAPIView.as_view(), name='trip-hotel-add'),
    url(r'^trip-road-add/', TripRoadAddAPIView.as_view(), name='trip-road-add'),
    url(r'^user-trips-list/', UserTripsListAPIView.as_view(), name='user-trip-list'),
    url(r'^user-trip-destroy/', TripDestroyAPIView.as_view(), name='trip-destroy'),
    # url(r'^user-hotels/', UserHotelsListAPIView.as_view(), name='user-hotels'),
    # url(r'^user-hotel-destroy/', UserHotelDestroyAPIView.as_view(), name='user-hotel-destroy'),


]
