from django.db import models

# Create your models here.
from User.models import User
from hotel.models import Hotel
from road.models import Road


class Trip(models.Model):
    name = models.CharField(max_length=300)
    start_lat = models.CharField(max_length=100)
    start_lon = models.CharField(max_length=100)
    start_name = models.CharField(max_length=100)
    end_lat = models.CharField(max_length=100)
    end_lon = models.CharField(max_length=100)
    end_name = models.CharField(max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    usage_date = models.DateField()
    created_date = models.DateTimeField(auto_now_add=True)

class TripHotel(models.Model):
    trip = models.ForeignKey(Trip, on_delete=models.CASCADE)
    hotel_id = models.CharField(max_length=10)
    city = models.CharField(max_length=100)
    hotel_number_trip = models.CharField(max_length=3)



class TripRoad(models.Model):
    trip = models.ForeignKey(Trip, on_delete=models.CASCADE)
    road_number_in_trip = models.CharField(max_length=3)
    road_type = models.CharField(max_length=3)
