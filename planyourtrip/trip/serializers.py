from rest_framework import serializers

from hotel.models import UserHotel, Hotel
from trip.models import Trip, TripHotel, TripRoad


class TripSerializer(serializers.ModelSerializer):
    # id = serializers.CharField(required=True)
    # name = serializers.CharField(required=True)
    # user = serializers.SerializerMethodField()

    class Meta:
        model = Trip
        fields = '__all__'

    # def get_user(self, context):
    #     return self.context['user']
    # print(self.context)


class TripHotelSerializer(serializers.ModelSerializer):
    hotel_id = serializers.CharField()
    trip_id = serializers.CharField()
    city = serializers.CharField()
    hotel_number_trip = serializers.CharField()

    class Meta:
        model = TripHotel
        fields = ['hotel_id', 'trip_id', 'city', 'hotel_number_trip']


class TripRoadSerializer(serializers.ModelSerializer):
    trip_id = serializers.CharField()
    road_type = serializers.CharField()
    road_number_in_trip = serializers.CharField()

    class Meta:
        model = TripRoad
        fields = ['trip_id', 'road_type', 'road_number_in_trip']


class UserTripsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trip
        fields = '__all__'
