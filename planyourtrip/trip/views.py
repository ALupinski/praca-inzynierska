# Create your views here.
from django.utils.dateparse import parse_date
from rest_framework import status
from rest_framework.generics import CreateAPIView, ListAPIView, DestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from User.models import User
from hotel.exceptions import UserDoesNotExist
from trip.exceptions import TripDoesNotExist
from trip.models import Trip, TripHotel, TripRoad
from trip.renderers import TripCreateJSONRenderer, TripRoadAddJSONRenderer, TripHotelAddJSONRenderer, \
    UserTripsListJSONRenderer, TripDestroyJSONRenderer
from trip.serializers import TripSerializer, TripHotelSerializer, TripRoadSerializer, UserTripsListSerializer


class TripCreateAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = TripSerializer
    renderer_classes = (TripCreateJSONRenderer,)

    def post(self, request, *args, **kwargs):
        data = request.data
        request_data = {
            'user': request.user.id,
            'name': data['name'],
            'start_lat': data['start_lat'],
            'start_lon': data['start_lon'],
            'start_name': data['start_name'],
            'end_lat': data['end_lat'],
            'end_lon': data['end_lon'],
            'end_name': data['end_name'],
            'usage_date': parse_date(data['usage_date']),

        }
        try:
            user = User.objects.get(id=request.user.id)
            # data['user_id'] = user_id
        except User.DoesNotExist:
            raise UserDoesNotExist

        trip_serializer = self.serializer_class(data=request_data)
        trip_serializer.is_valid(raise_exception=True)
        trip_serializer.save()

        return Response(trip_serializer.data, status=status.HTTP_201_CREATED)


class TripHotelAddAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = TripHotelSerializer
    renderer_classes = (TripHotelAddJSONRenderer,)

    def post(self, request, *args, **kwargs):
        data = request.data

        trip_serializer = self.serializer_class(data=data)
        trip_serializer.is_valid(raise_exception=True)
        trip_serializer.save()

        return Response(trip_serializer.data, status=status.HTTP_201_CREATED)


class TripRoadAddAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = TripRoadSerializer
    renderer_classes = (TripRoadAddJSONRenderer,)

    def post(self, request, *args, **kwargs):
        data = request.data

        trip_serializer = self.serializer_class(data=data)
        trip_serializer.is_valid(raise_exception=True)
        trip_serializer.save()

        return Response(trip_serializer.data, status=status.HTTP_201_CREATED)


class UserTripsListAPIView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserTripsListSerializer
    queryset = Trip.objects.all()
    renderer_classes = (UserTripsListJSONRenderer,)

    def list(self, request, *args, **kwargs):
        user_id = request.user.id
        queryset = self.get_queryset().filter(user_id=user_id)

        serializer = self.serializer_class(queryset, many=True)

        for trip in serializer.data:
            try:
                hotels = TripHotel.objects.filter(trip_id=trip['id'])
                trip_hotel_serializer = TripHotelSerializer(hotels, many=True)
                hotels = trip_hotel_serializer.data
            except TripHotel.DoesNotExist:
                hotels = []

            try:
                roads = TripRoad.objects.filter(trip_id=trip['id'])
                trip_road_serializer = TripRoadSerializer(roads, many=True)
                roads = trip_road_serializer.data
            except TripRoad.DoesNotExist:
                roads = []
            trip['hotels'] = hotels
            trip['roads'] = roads

        return Response(serializer.data, status=status.HTTP_200_OK)


class TripDestroyAPIView(DestroyAPIView):
    permission_classes = (IsAuthenticated,)
    # serializer_classes = AppointmentDestroySerializer
    renderer_classes = (TripDestroyJSONRenderer,)

    def delete(self, request, *args, **kwargs):

        try:
            trip_id = request.data['trip_id']
            trip = Trip.objects.get(id=trip_id)
        except Trip.DoesNotExist:
            raise TripDoesNotExist

        trip.delete()

        return Response(status=status.HTTP_200_OK)
