from planyourtrip.renderers import CustomJSONRenderer


class TripCreateJSONRenderer(CustomJSONRenderer):
    object_label = 'trip'
    message = 'Zapisano wycieczkę.'


class TripRoadAddJSONRenderer(CustomJSONRenderer):
    object_label = 'trip_road'
    message = 'Dodane trasę do wycieczki.'


class TripHotelAddJSONRenderer(CustomJSONRenderer):
    object_label = 'trip_hotel'
    message = 'Dodano hotel do wycieczki.'


class UserTripsListJSONRenderer(CustomJSONRenderer):
    object_label = 'user_trips'
    message = 'Lista zapisanych wycieczek:'


class TripDestroyJSONRenderer(CustomJSONRenderer):
    object_label = 'trip'
    message = 'Wycieczka została usunięta.'
