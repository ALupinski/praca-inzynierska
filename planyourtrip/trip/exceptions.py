from rest_framework.exceptions import APIException


class TripDoesNotExist(APIException):
    status_code = 400
    default_detail = 'Wybrana wycieczka nie istnieje'
