from django.conf.urls import url
from .views import ProfileApiView



app_name = 'Profile'

urlpatterns = [
    url(r'^profiles/(?P<id>\w+)/?$', ProfileApiView.as_view(), name='profile'),
]