from django.db import models


# Create your models here.



class Profile(models.Model):
    user = models.OneToOneField('User.User', on_delete=models.CASCADE)
    image = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.user.user_login



