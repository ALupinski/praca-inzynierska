from rest_framework import serializers

from hotel.models import UserHotel, Hotel


class HotelSerializer(serializers.ModelSerializer):
    # id = serializers.CharField(required=True)
    # name = serializers.CharField(required=True)
    class Meta:
        model = Hotel
        fields = '__all__'


class HotelUserSerializer(serializers.ModelSerializer):
    hotel_id = serializers.CharField()
    user_id = serializers.CharField()
    usage_date = serializers.CharField()

    class Meta:
        model = UserHotel
        fields = ['hotel_id', 'user_id', 'usage_date']


class UserHotelsListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Hotel
        fields = '__all__'
