from planyourtrip.renderers import CustomJSONRenderer


class HotelCreateJSONRenderer(CustomJSONRenderer):
    object_label = 'user_hotel'
    message = 'Zapisano Twój hotel.'


class UserHotelsListJSONRenderer(CustomJSONRenderer):
    object_label = 'user_hotels'
    message = 'Lista zapisanych hoteli:'


class HotelDestroyJSONRenderer(CustomJSONRenderer):
    object_label = 'user_hotels'
    message = 'Hotel został usunięty z Twojego konta'
