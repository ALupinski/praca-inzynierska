from rest_framework.exceptions import APIException


class UserDoesNotExist(APIException):
    status_code = 400
    default_detail = 'Wybrany użytkownik nie istnieje.'

class UserHotelExists(APIException):
    status_code = 400
    default_detail = 'Wybrany hotel jest już przypisany do Twojego konta'

class UserHotelDoesNotExist(APIException):
    status_code = 400
    default_detail = 'Wybrany hotel nie jest przypisany do Twojego konta'

class HotelDoesNotExist(APIException):
    status_code = 400
    default_detail = 'Wybrany hotel nie istnieje'
