from django.shortcuts import render
from django.utils.dateparse import parse_date

# Create your views here.
from rest_framework import status
from rest_framework.generics import CreateAPIView, ListAPIView, DestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from User.models import User
from hotel.exceptions import UserDoesNotExist, UserHotelExists, UserHotelDoesNotExist, HotelDoesNotExist
from hotel.models import Hotel, UserHotel
from hotel.renderers import HotelCreateJSONRenderer, UserHotelsListJSONRenderer, \
    HotelDestroyJSONRenderer
from hotel.serializers import HotelSerializer, HotelUserSerializer, UserHotelsListSerializer


class HotelCreateAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    # hotel_serializer_class = HotelSerializer
    # user_hotel_serializer_class = HotelUserSerializer
    renderer_classes = (HotelCreateJSONRenderer,)

    def post(self, request, *args, **kwargs):
        data = request.data
        user_id = request.user.id
        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            raise UserDoesNotExist

        hotel_id = request.data['id']
        try:
            hotel = Hotel.objects.get(id=hotel_id)
        except Hotel.DoesNotExist:

            hotel_serializer = HotelSerializer(data=data)
            hotel_serializer.is_valid(raise_exception=True)
            hotel_serializer.save()
            hotel = Hotel.objects.get(id=hotel_id)

        hotel_id = hotel.id
        usage_date = data['usage_date']
        hotel_user_data = {
            'hotel_id': hotel_id,
            'user_id': user_id,
            'usage_date': usage_date
        }

        try:
            user_hotel = UserHotel.objects.get(user_id=user_id, hotel_id=hotel_id, usage_date=usage_date)
            raise UserHotelExists
        except UserHotel.DoesNotExist:
            hotel_user_serializer = HotelUserSerializer(data=hotel_user_data)
            hotel_user_serializer.is_valid(raise_exception=True)
            hotel_user_serializer.save()



        return Response(hotel_user_serializer.data, status=status.HTTP_201_CREATED)


class UserHotelsListAPIView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserHotelsListSerializer
    queryset = UserHotel.objects.all()
    renderer_classes = (UserHotelsListJSONRenderer,)

    def list(self, request, *args, **kwargs):
        user_id = request.user.id
        queryset = self.get_queryset().filter(user_id=user_id)
        user_hotels = []
        for q in queryset:
            user_hotels.append(Hotel.objects.get(id=q.hotel_id))

        serializer = self.serializer_class(user_hotels, many=True)
        for hotel in serializer.data:
            hotel_usage_date = UserHotel.objects.get(hotel_id=hotel['id']).usage_date
            hotel['usage_date'] = str(hotel_usage_date)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class UserHotelDestroyAPIView(DestroyAPIView):
    permission_classes = (IsAuthenticated,)
    # serializer_classes = AppointmentDestroySerializer
    renderer_classes = (HotelDestroyJSONRenderer,)

    def delete(self, request, *args, **kwargs):

        try:
            hotel = Hotel.objects.get(id=request.data['hotel_id'])
            user_hotel = UserHotel.objects.get(user=request.user, hotel=hotel)
        except Hotel.DoesNotExist:
            raise HotelDoesNotExist
        except UserHotel.DoesNotExist:
            raise UserHotelDoesNotExist

        user_hotel.delete()

        return Response(status=status.HTTP_200_OK)
