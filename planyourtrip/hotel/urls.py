from django.conf.urls import url
from rest_framework import routers

from hotel.views import HotelCreateAPIView, UserHotelsListAPIView, UserHotelDestroyAPIView

app_name = 'hotel'

urlpatterns = [
    url(r'^user-hotel-create/', HotelCreateAPIView.as_view(), name='add-user-hotel'),
    url(r'^user-hotels/', UserHotelsListAPIView.as_view(), name='user-hotels'),
    url(r'^user-hotel-destroy/', UserHotelDestroyAPIView.as_view(), name='user-hotel-destroy'),


]
