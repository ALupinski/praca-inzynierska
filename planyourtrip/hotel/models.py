from django.db import models

# Create your models here.
from User.models import User
from comment.models import Comment


class Hotel(models.Model):
    id = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    lon = models.CharField(max_length=10, blank=True, null=True)
    lat = models.CharField(max_length=10, blank=True, null=True)
    floors = models.CharField(max_length=20, blank=True, null=True)
    rooms = models.CharField(max_length=20, blank=True, null=True)
    quality = models.CharField(max_length=50, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    fax = models.CharField(max_length=20, blank=True, null=True)
    amenity = models.TextField(max_length=400, blank=True, null=True)
    description = models.TextField(max_length=400, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)


class UserHotel(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    usage_date = models.DateField()
    created_date = models.DateTimeField(auto_now_add=True)


  


class CommentHotel(models.Model):
    hotel_id =  models.CharField(max_length=20, blank=True, null=True)
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('comment', 'hotel_id')
