from django.conf.urls import url
from rest_framework import routers

from hotel.views import HotelCreateAPIView, UserHotelsListAPIView, UserHotelDestroyAPIView
from road.views import RoadCreateAPIView, UserRoadsListAPIView, UserRoadDestroyAPIView

app_name = 'road'

urlpatterns = [
    url(r'^user-road-create/', RoadCreateAPIView.as_view(), name='add-user-road'),
    url(r'^user-roads/', UserRoadsListAPIView.as_view(), name='user-roads'),
    url(r'^user-road-destroy/', UserRoadDestroyAPIView.as_view(), name='user-road-destroy'),


]
