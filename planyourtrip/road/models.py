from django.db import models

# Create your models here.
from User.models import User
from comment.models import Comment

class Road(models.Model):
    road_id = models.CharField(max_length=50, primary_key=True, unique=True)
    sPointName = models.CharField(max_length=255)
    ePointName = models.CharField(max_length=255)
    slon = models.CharField(max_length=10)
    slat = models.CharField(max_length=10)
    elon = models.CharField(max_length=10)
    elat = models.CharField(max_length=10)
    geometryName = models.CharField(max_length=2)
    created_date = models.DateTimeField(auto_now_add=True)

    # class Meta:
    # unique_together = ('slon', 'slat', 'elon', 'elat', 'geometryName')


class UserRoad(models.Model):
    name = models.CharField(max_length=100)
    road = models.ForeignKey(Road, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    usage_date = models.DateField()
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('road', 'user')


class CommentRoad(models.Model):
    road_id = models.CharField(max_length=50, blank=True, null=True)
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('comment', 'road_id')
