from rest_framework.exceptions import APIException


class UserDoesNotExist(APIException):
    status_code = 400
    default_detail = 'Wybrany użytkownik nie istnieje.'


class UserRoadExists(APIException):
    status_code = 400
    default_detail = 'Wybrana trasa jest już przypisana do Twojego konta'


class UserRoadDoesNotExist(APIException):
    status_code = 400
    default_detail = 'Wybrana trasa nie jest przypisana do Twojego konta'

class RoadDoesNotExist(APIException):
    status_code = 400
    default_detail = 'Wybrana trasa nie istnieje'
