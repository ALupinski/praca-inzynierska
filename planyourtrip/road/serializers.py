from rest_framework import serializers

from hotel.models import UserHotel, Hotel
from road.models import Road, UserRoad


class RoadSerializer(serializers.ModelSerializer):
    # id = serializers.CharField(read_only=True)
    # name = serializers.CharField(required=True)
    class Meta:
        model = Road
        fields = ['road_id','sPointName', 'ePointName', 'slon', 'slat','elon', 'elat','geometryName']

class RoadUserSerializer(serializers.ModelSerializer):
    road_id = serializers.CharField()
    user_id = serializers.CharField()
    name = serializers.CharField()
    usage_date = serializers.CharField()


    class Meta:
        model = UserRoad
        fields = ['road_id', 'user_id', 'name','usage_date']


class UserRoadsListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Road
        fields = '__all__'


