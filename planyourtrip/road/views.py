# Create your views here.

from django.utils.dateparse import parse_date
from rest_framework import status
from rest_framework.generics import CreateAPIView, ListAPIView, DestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from User.models import User
from hotel.exceptions import UserDoesNotExist
from road.exceptions import UserRoadDoesNotExist, UserRoadExists, RoadDoesNotExist
from road.models import Road, UserRoad
from road.renderers import UserRoadsListJSONRenderer, RoadCreateJSONRenderer, RoadDestroyJSONRenderer
from road.serializers import UserRoadsListSerializer, RoadUserSerializer, RoadSerializer
from datetime import datetime

class RoadCreateAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    # hotel_serializer_class = HotelSerializer
    # user_hotel_serializer_class = HotelUserSerializer
    renderer_classes = (RoadCreateJSONRenderer,)

    def post(self, request, *args, **kwargs):
        data = request.data
        user_id = request.user.id
        name = data['name']
        slon = data['slon']
        slat = data['slat']
        elon = data['elon']
        elat = data['elat']
        geometryName = data['geometryName']
        usage_date = data['usage_date']
        # date = datetime.strptime(usage_date, '%Y %m %d').date()
        road_id = str(slon) + str(slat) + str(elon) + str(elat) + str(geometryName)
        print(usage_date)
        print(road_id)
        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            raise UserDoesNotExist

        # road_id = request.data['id']

        try:
            road = Road.objects.get(
                road_id=road_id,
                # slon=slon,
                # slat=slat,
                # elon=elon,
                # elat=elat,
                # geometryName=geometryName,
            )
        except Road.DoesNotExist:

            road_serializer = RoadSerializer(data=data)
            road_serializer.is_valid(raise_exception=True)
            road_serializer.save()
            road = Road.objects.get(road_id=road_serializer.data['road_id'])

        road_id = road.road_id
        road_user_data = {
            'road_id': road_id,
            'user_id': user_id,
            'name': name,
            'usage_date': usage_date
        }

        try:
            user_road = UserRoad.objects.get(user_id=user_id, road_id=road_id)
            raise UserRoadExists
        except UserRoad.DoesNotExist:

            road_user_serializer = RoadUserSerializer(data=road_user_data)
            road_user_serializer.is_valid(raise_exception=True)
            road_user_serializer.save()

        return Response(road_user_serializer.data, status=status.HTTP_201_CREATED)


class UserRoadsListAPIView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserRoadsListSerializer
    queryset = UserRoad.objects.all()
    renderer_classes = (UserRoadsListJSONRenderer,)

    def list(self, request, *args, **kwargs):
        user_id = request.user.id
        queryset = self.get_queryset().filter(user_id=user_id)
        user_roads = []
        for q in queryset:
            user_roads.append(Road.objects.get(road_id=q.road_id))
            print(user_roads)

        serializer = self.serializer_class(user_roads, many=True)
        # response = []
        for road in serializer.data:
            road_name = UserRoad.objects.get(road_id=road['road_id']).name
            road_usage_date = UserRoad.objects.get(road_id=road['road_id']).usage_date
            road['name'] = road_name
            road['usage_date'] = str(road_usage_date)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class UserRoadDestroyAPIView(DestroyAPIView):
    permission_classes = (IsAuthenticated,)
    # serializer_classes = AppointmentDestroySerializer
    renderer_classes = (RoadDestroyJSONRenderer,)

    def delete(self, request, *args, **kwargs):

        try:
            road = Road.objects.get(id=request.data['road_id'])
            user_road = UserRoad.objects.get(user=request.user, road=road)
        except Road.DoesNotExist:
            raise RoadDoesNotExist
        except UserRoad.DoesNotExist:
            raise UserRoadDoesNotExist

        user_road.delete()

        return Response(status=status.HTTP_200_OK)
