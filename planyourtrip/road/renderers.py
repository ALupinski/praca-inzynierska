from planyourtrip.renderers import CustomJSONRenderer


class RoadCreateJSONRenderer(CustomJSONRenderer):
    object_label = 'user_hotel'
    message = 'Zapisano Twoją trasę.'


class UserRoadsListJSONRenderer(CustomJSONRenderer):
    object_label = 'user_road'
    message = 'Lista zapisanych tras:'


class RoadDestroyJSONRenderer(CustomJSONRenderer):
    object_label = 'user_hotels'
    message = 'Trasa została usunięta z Twojego konta'
